import * as CONSTANTS from '../constants';

export const getServices = () => ({ type: CONSTANTS.GET_SERVICES });
export const receiveServices = (services) => ({
  type: CONSTANTS.RECEIVE_SERVICES,
  services,
});

export const getServicesById = (svid) => ({
  type: CONSTANTS.GET_SERVICES_BY_ID,
  svid,
})
export const receiveServicesById = (service) => ({
  type: CONSTANTS.RECEIVE_SERVICES_BY_ID,
  service,
})

export const createService = (data) => ({
  type: CONSTANTS.CREATE_SERVICE,
  data,
});
export const receiveCreateServices = (message) => ({
  type: CONSTANTS.RECEIVE_CREATE_SERVICES,
  message,
});

export const updateService = (data) => ({
  type: CONSTANTS.UPDATE_SERVICE,
  data,
});
export const receiveUpdateServices = (message) => ({
  type: CONSTANTS.RECEIVE_UPDATE_SERVICES,
  message,
});

export const changeServiceStatus = (data) => ({
  type: CONSTANTS.CHANGE_SERVICE_STATUS,
  data,
});




