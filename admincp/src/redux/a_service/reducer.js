import * as CONSTANTS from '../../redux/constants';

export default (state = {}, { type, services, service, message }) => {
  switch (type) {
    case CONSTANTS.RECEIVE_SERVICES:
      return {
        services,
      };
    case CONSTANTS.RECEIVE_SERVICES_BY_ID:
      return {
        service,
      };
    case CONSTANTS.RECEIVE_CREATE_SERVICES:
      return {
        message,
      };
    case CONSTANTS.RECEIVE_UPDATE_SERVICES:
      return {
        ...state, message,
      };
    default:
      return state;
  }
};
