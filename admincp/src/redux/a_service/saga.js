import { call, put, takeLatest, all } from 'redux-saga/effects';
import * as CONSTANTS from '../constants';
import * as ACTIONS from './action';
import * as API from '../../services/api';
import { changeOrderStatus } from '../order/action';

const env = process.env.REACT_APP_ENV || process.env.NODE_ENV || 'development';
const config = require('../../config/config')[env];

function* getService(action) {
  try {
    const link = config.base_api + '/service';
    const services = yield call(API.getData, link);
    yield put(ACTIONS.receiveServices(services));
  } catch (e) {
    console.log(e);
  }
}

function* gerServiceById(action) {
  const {svid} =  action;
  try {
    const link = config.base_api + '/service/' + svid;
    const service = yield call(API.getData, link);
    yield put(ACTIONS.receiveServicesById(service));
  } catch (e) {
    console.log(e);
  }
}

function* createService(action) {
  const {data} =  action;
  try {
    const link = config.base_api + '/service';
    const result = yield call(API.postData, data, link);
    yield put(ACTIONS.receiveCreateServices(result['message']));
  } catch (e) {
    console.log(e);
  }
}

function* updateService(action) {
  const {data} =  action;
  try {
    if(data.svid){
      const link = config.base_api + '/service/' + data.svid;
      const result = yield call(API.putData, data.service, link);
      yield put(ACTIONS.receiveUpdateServices(result['message']));
    } else{
      const listSvid = data.listSvid;
      const changeTopic = data.changeTopic;
      const res = yield all(
        listSvid.map((item, index) => {
          const link = config.base_api + '/service/' + item;
          return call(API.putData, {topic: changeTopic[index]} , link);
        })
      );
      yield put(ACTIONS.receiveUpdateServices(res[0]['message']));
    }
  } catch (e) {
    console.log(e);
  }
}

function* changeServiceStatus(action) {
  const { data } = action;
  try {
    const res = yield all(
      data.map((item) => {
        const link = config.base_api + '/service/' + item.id;
        const update = { status: item.status };
        return call(API.putData, update, link);
      })
    );
    yield put(ACTIONS.receiveUpdateServices(res[0]['message']));
  } catch (e) {
    console.log(e);
  }
}

export default function* rootSaga() {
  yield takeLatest(CONSTANTS.GET_SERVICES, getService);
  yield takeLatest(CONSTANTS.GET_SERVICES_BY_ID, gerServiceById);
  yield takeLatest(CONSTANTS.CREATE_SERVICE, createService);
  yield takeLatest(CONSTANTS.UPDATE_SERVICE, updateService);
  yield takeLatest(CONSTANTS.CHANGE_SERVICE_STATUS, changeServiceStatus);
}
