export const getUserProfile = (users, profiles) => {
  const luser = users.length;
  const lprofile = profiles.length;
  let results = [];

  for(let i = 0; i< luser; i++){
    let n = true;
    for(let j = 0; j< lprofile; j++){
      if(users[i].uid == profiles[j].uid){
        results.push({ ...users[i], ...profiles[j] })
        n = false;
        break;
      }
    }
    if(n)  results.push({...users[i]});
  }

  return results;
};