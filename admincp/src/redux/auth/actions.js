const actions = {
  CHECK_AUTHORIZATION: 'CHECK_AUTHORIZATION',
  LOGIN_REQUEST: 'LOGIN_REQUEST',
  LOGOUT: 'LOGOUT',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGIN_ERROR: 'LOGIN_ERROR',
  GET_AVATAR: 'GET_AVATAR',
  RECIVE_AVATAR: 'RECIVE_AVATAR',
  checkAuthorization: () => ({ type: actions.CHECK_AUTHORIZATION }),
  login: (username, password) => ({
    type: actions.LOGIN_REQUEST,
    username,
    password,
  }),
  logout: () => ({
    type: actions.LOGOUT,
  }),
  getAvatarAdmin: (uid) => ({
    type: actions.GET_AVATAR,
    uid,
  }),
};
export default actions;

