import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { getToken, getUid, clearToken } from '../../helpers/utility';
import actions from './actions';
import * as API from '../../services/api';
import { history } from '../store';

const env = process.env.REACT_APP_ENV || process.env.NODE_ENV || 'development';
const config = require('../../config/config')[env];

export function* loginRequest() {
  yield takeEvery('LOGIN_REQUEST', function* (action) {
    const { username, password } = action;
    try {
      const data = {
        email: username,
        password: password,
        Phone: password,
      };
      const link = config.base_api + '/login';
      const result = yield call(API.postData, data, link);
      if (result['status'] == 'SUCCESS') {
        if (
          result.data['type'] == 'ROLE_MANAGER' ||
          result.data['type'] == 'ROLE_GENERALMANAGER'
        ) {
          localStorage.setItem('id_token', result['message']);
          localStorage.setItem('uid', result['data']['uid']);
          yield put({
            type: actions.LOGIN_SUCCESS,
            token: result['message'],
            uid: result['data']['uid'],
          });
          history.push('/dashboard');
        } else {
          yield put({ type: actions.LOGIN_ERROR, error: 'Account Failed' });
        }
      } else {
        yield put({
          type: actions.LOGIN_ERROR,
          error: result['message'],
        });
      }
    } catch (e) {
      yield put({
        type: actions.LOGIN_ERROR,
        error: 'Wrong email or password',
      });
    }
  });
}

export function* loginSuccess() {
  yield takeEvery(actions.LOGIN_SUCCESS, function* (payload) {
    yield localStorage.setItem('id_token', payload.token);
  });
}

export function* loginError() {
  yield takeEvery(actions.LOGIN_ERROR, function* () { });
}

export function* logout() {
  yield takeEvery(actions.LOGOUT, function* () {
    clearToken();
    yield put(push('/'));
  });
}

export function* getAvatarAdmin() {
  yield takeEvery('GET_AVATAR', function* (action) {
    const { uid } = action;
    try {
      const data = {
        uid: uid,
        isAvatar: 1,
      };
      const link = config.base_api + '/images/sv';
      const result = yield call(API.postData, data, link);
      yield put({
        type: actions.RECIVE_AVATAR,
        avatar: result,
      })
    } catch (e) {
      console.log(e);
    }
  });
}

export function* checkAuthorization() {
  yield takeEvery(actions.CHECK_AUTHORIZATION, function* () {
    const token = getToken().get('idToken');
    const uid = getUid().get('uid');
    if (token) {
      yield put({
        type: actions.LOGIN_SUCCESS,
        token,
        uid,
      });
    }
  });
}

export default function* rootSaga() {
  yield all([
    fork(checkAuthorization),
    fork(loginRequest),
    fork(loginSuccess),
    fork(loginError),
    fork(logout),
    fork(getAvatarAdmin),
  ]);
}
