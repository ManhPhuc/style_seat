import actions from './actions';
import { join } from 'redux-saga/effects';

const initState = { idToken: null };

export default function authReducer(state = initState, action) {
  switch (action.type) {
    case actions.LOGIN_SUCCESS:
      return {idToken: action.token, uid: action.uid };
    case actions.LOGOUT:
      return initState;
    case actions.LOGIN_ERROR:
      return { error: action.error };
    case actions.RECIVE_AVATAR:
      return { avatar: action.avatar };
    default:
      return state;
  }
}
