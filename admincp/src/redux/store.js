import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import {createLogger} from 'redux-logger'
// import {createHistory} from  'history/createBrowserHistory' ;
import { createBrowserHistory } from 'history';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';
import reducers from '../redux/reducers';
import rootSaga from '../redux/sagas';

const history = createBrowserHistory({ basename: '/admincp/' });
const sagaMiddleware = createSagaMiddleware();
const routeMiddleware = routerMiddleware(history);
const loggerMiddleware = createLogger();
const middlewares = [sagaMiddleware, routeMiddleware, loggerMiddleware];


const store = createStore(
  combineReducers({
    ...reducers,
    router: routerReducer,
  }),
  compose(applyMiddleware(...middlewares))
);
sagaMiddleware.run(rootSaga)

export { store, history };

