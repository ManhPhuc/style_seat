import * as CONSTANTS from '../../redux/constants';

export default (state = {}, { type, users, user, message }) => {
  switch (type) {
    case CONSTANTS.RECEIVE_USERS:
      return {
        users,
      };
    case CONSTANTS.RECEIVE_USERS_BY_ID:
      return {
        user,
      };
    case CONSTANTS.RECEIVE_UPDATE_USERS:
      return {
        ...state,
        message,
      };
    case CONSTANTS.RECEIVE_ADD_USERS:
      return {
        message,
      };
    default:
      return state;
  }
};
