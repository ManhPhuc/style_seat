import { call, put, all, takeLatest } from 'redux-saga/effects';
import * as CONSTANTS from '../constants';
import * as ACTIONS from './action';
import * as API from '../../services/api';
import { getUserProfile } from '../apiCall';

const env = process.env.REACT_APP_ENV || process.env.NODE_ENV || 'development';
const config = require('../../config/config')[env];

function* getUsers(action) {
  try {
    const link = config.base_api + '/users';
    const link2 = config.base_api + '/profiles';
    const [users, profiles] = yield all([
      call(API.getData, link),
      call(API.getData, link2),
    ]);
    const result = yield call(getUserProfile, users, profiles);
    yield put(ACTIONS.receiveUsers(result));
  } catch (e) {
    console.log(e);
  }
}

function* getUserById(action) {
  const { uid } = action;
  try {
    const link = config.base_api + '/users/' + uid;
    const link2 = config.base_api + '/profiles/' + uid;
    const [user, profile] = yield all([
      call(API.getData, link),
      call(API.getData, link2),
    ]);
    const result = ({ ...user['data'], ...profile['data'] });
    yield put(ACTIONS.receiveUserById(result));
  } catch (e) {
    console.log(e);
  }
}

function* updateUser(action) {
  const { data } = action;
  try {
    const link = config.base_api + '/users/' + data.uid;
    const link2 = config.base_api + '/admin/profiles/' + data.uid;
    const [users, profiles] = yield all([
      call(API.putData, data.user, link),
      call(API.putData, data.profile, link2),
    ]);
    yield put(ACTIONS.receiveUpdateUsers(users['message']));
  } catch (e) {
    console.log(e);
  }
}

function* addUser(action) {
  const { data } = action;
  try {
    const link = config.base_api + '/users';
    const result = yield call(API.postData, data, link);
    yield put(ACTIONS.receiveAddUser(result['message']));
  } catch (e) {
    console.log(e);
  }
}

function* changeUserStatus(action) {
  const { data } = action;
  try {
    const res = yield all(
      data.map((item) => {
        const link = config.base_api + '/users/' + item.id;
        const update = { status: item.status, type: item.type };
        return call(API.putData, update, link);
      })
    );
    yield put(ACTIONS.receiveUpdateUsers(res[0]['message']));
  } catch (e) {
    console.log(e);
  }
}

export default function* rootSaga() {
  yield takeLatest(CONSTANTS.GET_USERS, getUsers);
  yield takeLatest(CONSTANTS.GET_USERS_BY_ID, getUserById);
  yield takeLatest(CONSTANTS.UPDATE_USERS, updateUser);
  yield takeLatest(CONSTANTS.ADD_USERS, addUser);
  yield takeLatest(CONSTANTS.CHANGE_USER_STATUS, changeUserStatus);
}
