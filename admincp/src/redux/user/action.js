import * as CONSTANTS from '../constants';

export const getUsers = () => ({ type: CONSTANTS.GET_USERS });
export const receiveUsers = (users) => ({
  type: CONSTANTS.RECEIVE_USERS,
  users,
});

export const getUserById = (uid) => ({
  type: CONSTANTS.GET_USERS_BY_ID,
  uid,
 });
export const receiveUserById = (user) => ({
  type: CONSTANTS.RECEIVE_USERS_BY_ID,
  user,
});

export const updateUsers = (data) => ({
  type: CONSTANTS.UPDATE_USERS,
  data,
});
export const receiveUpdateUsers = (message) => ({
  type: CONSTANTS.RECEIVE_UPDATE_USERS,
  message,
});

export const addUser = (data) => ({
  type: CONSTANTS.ADD_USERS,
  data,
});
export const receiveAddUser = (message) => ({
  type: CONSTANTS.RECEIVE_ADD_USERS,
  message,
});

export const changeUserStatus = (data) => ({
  type: CONSTANTS.CHANGE_USER_STATUS,
  data,
});

