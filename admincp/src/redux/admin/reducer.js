import * as CONSTANTS from '../../redux/constants';

export default (state = {}, { type, profile, avatar, message }) => {
  switch (type) {
    case CONSTANTS.RECEIVE_PROFILE:
      return {
        profile,
      };
    case CONSTANTS.RECEIVE_UPDATE_ADMIN:
      return {
        message,
      };
    case CONSTANTS.RECEIVE_IMAGES:
      return {
        avatar,
      };
    default:
      return state;
  }
};
