import * as CONSTANTS from '../constants';

export const getProfile = (uid) => ({ type: CONSTANTS.GET_PROFILE, uid: uid });
export const updateProfile = (data) => ({
  type: CONSTANTS.UPDATE_PROFILE,
  data,
});
export const receiveProfile = (profile) => ({
  type: CONSTANTS.RECEIVE_PROFILE,
  profile,
});
export const receiveUpdateProfile = (message) => ({
  type: CONSTANTS.RECEIVE_UPDATE_ADMIN,
  message,
});

export const getAvatar = (uid) => ({ type: CONSTANTS.GET_AVATAR, uid: uid });
export const receiveImages = (avatar) => ({
  type: CONSTANTS.RECEIVE_IMAGES,
  avatar,
});
