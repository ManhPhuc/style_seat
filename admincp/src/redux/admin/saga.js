import { call, all, put, takeLatest } from 'redux-saga/effects';
import * as CONSTANTS from '../constants';
import * as ACTIONS from './action';
import * as API from '../../services/api';

const env = process.env.REACT_APP_ENV || process.env.NODE_ENV || 'development';
const config = require('../../config/config')[env];

function* getProfile(action) {
  const { uid } = action;
  try {
    const link = config.base_api + '/profiles/' + uid;
    const link2 = config.base_api + '/users/' + uid;
    const [users, profiles] = yield all([
      call(API.getData, link2),
      call(API.getData, link),
    ]);
    const result ={ ...users['data'], ...profiles['data'] };
    yield put(ACTIONS.receiveProfile(result));
  } catch (e) {
    console.log(e);
  }
}

function* updateAdmin(action) {
  const { data } = action;
  try {
    const link = config.base_api + '/users/';
    const link2 = config.base_api + '/profiles/';
    const [users, profiles] = yield all([
      call(API.putData, data.user, link),
      call(API.putData, data.profile, link2),
    ]);
    yield put(ACTIONS.receiveUpdateProfile(users['message']));
  } catch (e) {
    console.log(e);
  }
}

function* getAvatar(action) {
  const { uid } = action;
  try {
    const data = {
      uid: uid,
      isAvatar: 1,
    };
    const link = config.base_api + '/images/sv';
    const result = yield call(API.postData, data, link);
    yield put(ACTIONS.receiveImages(result));
  } catch (e) {
    console.log(e);
  }
}

export default function* rootSaga() {
  yield takeLatest(CONSTANTS.GET_AVATAR, getAvatar);
  yield takeLatest(CONSTANTS.GET_PROFILE, getProfile);
  yield takeLatest(CONSTANTS.UPDATE_PROFILE, updateAdmin);
}
