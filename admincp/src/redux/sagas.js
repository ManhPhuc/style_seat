import authSagas from './auth/saga';
import orderSagas from './order/saga';
import serviceSagas from './a_service/saga';
import userSagas from './user/saga';
import adminSagas from './admin/saga';
import notiSaga from './notification/saga';
import { all } from 'redux-saga/effects';

export default function* rootSaga() {
  yield all([
    authSagas(),
    orderSagas(),
    serviceSagas(),
    userSagas(),
    adminSagas(),
    notiSaga()
  ]);
}
