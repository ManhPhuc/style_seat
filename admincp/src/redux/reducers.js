import Auth from './auth/reducer';
import App from './app/reducer';
import order from './order/reducer';
import services from './a_service/reducer';
import users from './user/reducer';
import admin from './admin/reducer';
import notification from './notification/reducer';

export default {
  order,
  Auth,
  App,
  services,
  users,
  admin,
  notification
};
