import { put, fork, take } from 'redux-saga/effects';
import * as CONSTANTS from '../constants';
import * as actions from './action';
import io from 'socket.io-client';
import { store } from '../store';

const env = process.env.REACT_APP_ENV || process.env.NODE_ENV || 'development';
const apiConfig = require('../../config/config')[env];

export function* establishSocketConnection() {
  while (true) {
    yield take('LOGIN_SUCCESS');
    const socket = io(`${apiConfig.socket_url}/admin`);
    socket.on(CONSTANTS.ACCOUNT_STATUS_CHANGE, (notiData) => store.dispatch(actions.accountNoti(notiData)));
    socket.on(CONSTANTS.SERVICE_STATUS_CHANGE,(notiData) => store.dispatch(actions.serviceNoti(notiData)));
    socket.on(CONSTANTS.ORDER_STATUS_CHANGE,(notiData) => store.dispatch(actions.orderNoti(notiData)));
  }
}
export default function* saga() {
  yield take(establishSocketConnection)
}
