import * as CONSTANTS from '../../redux/constants';

export default (state = {
  user: {
    count: 0,
    notiData: []
  },
  services: {
    count: 0,
    notiData: []
  },
  order: {
    count: 0,
    notiData: []
  }
}, { type, notiData }) => {
  switch (type) {
    case CONSTANTS.ACCOUNT_STATUS_CHANGE: {
      state.user.notiData.push(notiData);
      return {
        services: { ...state.services },
        order: { ...state.order },
        user: {
          count: state.user.count + 1,
          notiData: state.user.notiData
        }
      }
    }
    case CONSTANTS.SERVICE_STATUS_CHANGE: {
      state.services.notiData.push(notiData);
      return {
        user: { ...state.user },
        order: { ...state.order },
        services: {
          count: state.services.count + 1,
          notiData: state.services.notiData
        }
      }
    }
    case CONSTANTS.ORDER_STATUS_CHANGE: {
      state.order.notiData.push(notiData);
      return {
        services: { ...state.services },
        user: { ...state.user },
        order: {
          count: state.order.count + 1,
          notiData: state.order.notiData
        }
      }
    }
    default:
      return state;
  }
};
