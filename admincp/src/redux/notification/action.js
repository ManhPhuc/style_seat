import * as CONSTANTS from '../constants';

export const accountNoti = (notiData) => ({ type: CONSTANTS.ACCOUNT_STATUS_CHANGE, notiData });
export const serviceNoti = (notiData) => ({ type: CONSTANTS.SERVICE_STATUS_CHANGE, notiData });
export const orderNoti = (notiData) => ({ type: CONSTANTS.ORDER_STATUS_CHANGE, notiData });