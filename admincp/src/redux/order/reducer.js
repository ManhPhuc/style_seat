import * as CONSTANS from '../constants';
import Item from 'antd/lib/list/Item';

export default (state = {}, { type, orders, message }) => {
  switch (type) {
    case CONSTANS.RECEIVE_ORDER:
      const data = [];
      if (orders !== undefined && orders[0] !== undefined) {
        orders.map(item => {
          let orderService = [];
          item.OrderServices.map(item2 => {
            orderService.push({
              osvid: item2.osvid,
              reviewContent: item2.reviewContent,
              reviewStar: item2.reviewStar,
              price: item2.WorkerService.price,
              time: item2.WorkerService.estimation,
              service: item2.WorkerService.Service.name,
              worker: item2.WorkerService.Profile.firstName + ' ' + item2.WorkerService.Profile.lastName,
            })
          })

          data.push({
            oid: item.oid,
            client: item.Profile.firstName + ' ' + item.Profile.lastName,
            use: item.customerName,
            time: item.date + '/' + item.timeStart,
            price: item.totalPrice,
            status: item.status + '-' + item.oid,
            note: item.note,
            issue: item.issue,
            accept: item.changeStatusNote,
            orderService: orderService,
          })
        })
      }

      return {
        orders: data,
      };
    case CONSTANS.RECEIVE_UPDATE_ORDER:
      return {
        ...state,
        message,
      };
    default:
      return state;
  }
};
