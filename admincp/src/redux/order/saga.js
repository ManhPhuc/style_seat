import { call, put, all, takeLatest } from 'redux-saga/effects';
import * as CONSTANTS from '../constants';
import * as ACTIONS from './action';
import * as API from '../../services/api';

const env = process.env.REACT_APP_ENV || process.env.NODE_ENV || 'development';
const config = require('../../config/config')[env];

function* getOrder(action) {
  try {
    const link = config.base_api + '/orders';
    const orders = yield call(API.getData, link);
    yield put(ACTIONS.receiveOrder(orders));
  } catch (e) {
    console.log(e);
  }
}

function* changeOrderStatus(action) {
  const { data } = action;
  try {
    const res = yield all(
      data.map((item) => {
        const link = config.base_api + '/orders/' + item.id;
        const link2 = config.base_api + '/order-services/' + item.id;
        if(item.status == 2){
          let order = {status: item.status, changeStatusNote: item.message}
          return call(API.putData, order , link);
        } else if(item.status == 3){
          return call(API.putData, {status: item.status, issue: item.message}, link);
        }
        // else {
        //   call(API.putData, {status: item.status, reviewContent: item.message}, link2);
        // }
        // console.log(item.status[0].value);
        // // const rs = yield call(API.putData,item.status[0].value, link);
        // yield put(result.push(rs));
        // console.log(result);

        // return call(API.putData, { status: item.status[0].value }, link);
      })
    );
    yield put(ACTIONS.receiveUpdateOrder(res[0]['message']));
    console.log(res);
  } catch (e) {
    console.log(e);
  }
}

export default function* rootSaga() {
  yield all([
    takeLatest(CONSTANTS.GET_ORDER, getOrder),
    takeLatest(CONSTANTS.CHANGE_ORDER_STATUS, changeOrderStatus),
  ]);
}
