import * as CONSTANTS from '../constants';

export const getOrder = () => ({ type: CONSTANTS.GET_ORDER });
export const receiveOrder = (orders) => ({
  type: CONSTANTS.RECEIVE_ORDER,
  orders,
});

export const changeOrderStatus = (data) => ({
  type: CONSTANTS.CHANGE_ORDER_STATUS,
  data,
});

export const receiveUpdateOrder = (message) => ({
  type: CONSTANTS.RECEIVE_UPDATE_ORDER,
  message,
});
