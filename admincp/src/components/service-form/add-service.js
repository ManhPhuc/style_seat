import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Select, Col, Form, Input, Button, Row, Modal } from 'antd';
import { createService } from '../../redux/a_service/action';
import { history } from '../../redux/store';

const status = [
  { key: 1, value: 'active', label: 'Kích hoạt', color: '#00BB00' },
  { key: 2, value: 'inactive', label: 'Khóa', color: '#FF0000' },
  { key: 3, value: 'pending', label: 'Chờ xử lý', color: '#FFAA00' },
  { key: 4, value: 'ban', label: 'Cấm', color: '#FF0000' },
];
const Option = Select.Option;
let url = window.location.pathname;
let url2 = url.replace('/add-service', '');
const url3 = url2.replace('/admincp', '');

class AddService extends Component {
  constructor() {
    super();
    this.state = {
      status: 0,
      visible: false,
      name: '',
    };
  }

  create = () => {
    const description = this._description.value;
    const price = this._price.state.value;
    const name = this._name.state.value;
    const estimation = this._estimation.state.value;
    const status = this.state.status;
    const data = { name, price, estimation, description, status }
    this.setState({ name: name, visible: true });
    this.props.createService(data);
  }

  getStatus = (e) => {
    this.setState({ status: e });
  }

  handleOk = (e) => {
    this.setState({
      visible: false,
    });
    history.push(url3);
  }

  handleCancel = (e) => {
    this.setState({
      visible: false,
    });
  }

  render() {
    return (
      <div>
        <Modal
          title="Thông Báo"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          {this.props.services.message == 'Create service successfully' ? <p>Thêm ' {this.state.name} ' thành công!</p> : <p>Thêm ' {this.state.name} ' thất bại!</p>}
        </Modal>
        <h3>Thêm Dịch Vụ</h3>
        <Form>
          <Row>
            <Col xs={20} xl={10} offset={1}>
              <Form.Item label="Tên:">
                <Input type="text" ref={Input => this._name = Input} />
              </Form.Item>
              <Form.Item label="Giá:">
                <Input type="text" ref={Input => this._price = Input} />
              </Form.Item>
            </Col>


            <Col xs={20} xl={10} offset={1}>
              <Form.Item label="Thời gian:">
                <Input type="text" ref={Input => this._estimation = Input} />
              </Form.Item>
              <Form.Item label="Trạng thái:">
                <Select onSelect={this.getStatus}>
                  {status.map((i) => (
                    <Option key={i.value}>
                      <p style={{ color: i.color }}>
                        {i.label}
                      </p>
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={22} offset={1}>
              <Form.Item label="Mô tả:">
                <textarea style={{ width: '96%', borderRadius: '3px' }} ref={textarea => this._description = textarea}></textarea>
              </Form.Item>
            </Col>
          </Row>
        </Form>

        <Col span={22} style={{ marginTop: '5%' }}>
          <Row type="flex" justify="center">
            <Col offset={2}>
              <Button size="default" type="primary" style={{ borderRadius: '4px' }}
                onClick={() => { this.create() }}>
                Lưu
              </Button>
            </Col>
            <Col offset={2}>
              <Button size="default" type="danger" style={{ borderRadius: '4px' }}
                onClick={() => history.push(url3)}
              >
                Hủy
              </Button>
            </Col>
          </Row>
        </Col>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { services } = state;
  return { services };
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      createService,
    },
    dispatch
  );

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddService);
export { connected as AddService };
