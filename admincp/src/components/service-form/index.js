import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getServices, changeServiceStatus, updateService } from '../../redux/a_service/action';
import { Table, Row, Col, Input, Button, Icon, Select, Spin, Empty, Tooltip, Modal, Popconfirm, Checkbox, Dropdown, Menu } from 'antd';
import { Link } from 'react-router-dom';
import Highlighter from 'react-highlight-words';
import { history } from '../../redux/store';
import './service-form.css';

const Option = Select.Option;
let selectChange = [];
const status = [
  { key: 'active', value: 1, label: 'Kích hoạt', color: '#00BB00' },
  { key: 'inactive', value: 2, label: 'Khóa', color: '#FF0000' },
  { key: 'pending', value: 3, label: 'Chờ xử lý', color: '#FFAA00' },
  { key: 'ban', value: 4, label: 'Cấm', color: '#FF0000' },
];
const topic = [
  { key: 1, value: 1, label: 'Yêu Thích', color: '#00BB00' },
  { key: 2, value: 2, label: 'Mùa Hè', color: '#00BB00' },
  { key: 4, value: 4, label: 'Cá Tính', color: '#00BB00' },
  { key: 8, value: 8, label: 'Mùa Xuân', color: '#00BB00' },
];
let checkAdd = false;
let listAdd = [];
let checkDel = false;
let listDel = [];
const CheckboxGroup = Checkbox.Group;

const handleStatusChange = (e) => {
  const change = e.split('-');
  const Cstatus = status.filter((option) => option.key === change[0]);
  const value = Cstatus[0].value;
  selectChange[change[1]] = {
    status: value,
    id: change[1],
  };
};
const urlEdit = '/dashboard/services/update-service';
const urlAdd = '/dashboard/services/add-service'

class ServiceTable extends Component {
  state = {
    searchText: '',
    bordered: true,
    loading: false,
    size: 'middle',
    visibleModal: false,
    selectedRowKeys: [],
  };

  // menuAdd = () => (
  //   <Menu>
  //     {topic.map((item) => (
  //       <Menu.Item key={item.key}>
  //         <Checkbox><span style={{ color: item.color }}>
  //           {item.label}
  //         </span></Checkbox>
  //       </Menu.Item>
  //     ))}
  //   </Menu>
  // );

  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys, selectedKeys, confirm, clearFilters,
    }) => (
        <div style={{ padding: 8 }}>
          <Input
            ref={node => { this.searchInput = node; }}
            placeholder={`Tìm ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
            onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
            style={{ width: 188, marginBottom: 8, display: 'block' }}
          />
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm)}
            icon="search"
            size="small"
            style={{ width: 90, marginRight: 8, borderRadius: '4px' }}
          >
            Tìm
        </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90, borderRadius: '4px' }}
          >
            Hủy
        </Button>
        </div>
      ),
    filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) => (
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    ),
  })

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: '' });
  }

  componentWillMount() {
    this.props.getServices();
  }

  handleOk = (e) => {
    this.setState({
      visibleModal: false,
    });
    this.props.getServices();
    window.location.reload();
  };

  handleCancel = (e) => {
    this.setState({
      visibleModal: false,
    });
    this.props.getServices();
    window.location.reload();
  };

  updateStatusService = () => {
    selectChange = selectChange.filter(function (n) { return n != undefined });
    if (selectChange[0]) {
      this.props.changeServiceStatus(selectChange);
      this.setState({ visibleModal: true });
    }
  }

  cancelService = () => {
    window.location.reload();
  }

  handleDelete = (id) => {

  }

  onSelectChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
    console.log('select: ' +  selectedRowKeys);
  }

  showTopic = (value) => {
    let selectTopic = [];
    let arrayTopic = [1, 2, 4, 8];
    arrayTopic.map(item => (value & item) == item ? selectTopic.push(item) : true);
    return selectTopic;
  }

  onCheckAddChange = (e) => {
    let list = JSON.stringify(e).replace('[','');
    list = list.replace(']','');
    listAdd = list.split(",");
  }

  onCheckDelChange = (e) => {
    let list = JSON.stringify(e).replace('[','');
    list = list.replace(']','');
    listDel = list.split(",");
  }

  saveAddTopic = () =>{
    const listSelect = this.state.selectedRowKeys;
    let changeTopic = [];
    let listSvid = [];
    listSelect.map(item => {
      const selected = item.split('-');
      const svid = parseInt(selected[0]);
      listSvid.push(svid)
      const topic = ((selected[1] !== 'null') && (selected[1] !== '0')) ?  parseInt(selected[1]) : 0;
      let topics = topic;
      let all = 0;
      listAdd.map(item2 =>{
        const nitem2 = parseInt(item2);
        all += nitem2;
        if(topic !== 0){
          (topic & nitem2) === nitem2 ? (topics = topics) : (topics += nitem2);
        }
      })
      if(topic === 0) topics = all;
      changeTopic.push(topics);
    })
    const data = {listSvid, changeTopic};
    this.props.updateService(data);
    this.setState({ visibleModal: true });
  }

  saveDelTopic= () =>{
    const listSelect = this.state.selectedRowKeys;
    let changeTopic = [];
    let listSvid = [];
    listSelect.map(item => {
      const selected = item.split('-');
      const svid = parseInt(selected[0]);
      listSvid.push(svid)
      const topic = ((selected[1] !== 'null') && (selected[1] !== '0')) ?  parseInt(selected[1]) : 0;
      let topics = topic;
      listDel.map(item2 =>{
        const nitem2 = parseInt(item2);
        if(topic !== 0){
          (topic & nitem2) === nitem2 ? (topics -= nitem2) : (topics = topics);
        }
      })
      changeTopic.push(topics);
    })
    const data = {listSvid, changeTopic};
    this.props.updateService(data);
    this.setState({ visibleModal: true });
  }

  render() {
    const { selectedRowKeys } = this.state;
    let data = [];
    const services = this.props.services.services;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    // const (this.state.selectedRowKeys.length > 0) = (this.state.selectedRowKeys.length > 0);

    if (services && services.results) {
      services.results.map((service, index) => {
        data.push({
          id: `${service.svid}`,
          key: `${service.svid}`+ '-' +`${service.topic}`,
          name: `${service.name}`,
          estimation: `${service.estimation}`,
          price: `${service.price}`,
          status: `${service.status}` + '-' + `${service.svid}`,
          description: service.description ? `${service.description}` : '',
          topic: `${service.topic}`,
        });
      });
    } else {
      return (
        <div>
          <Spin>
            <Empty />
          </Spin>
        </div>
      );
    }

    const title = () => (
      <div>
        <Row type="flex" >
          <Col span={1}></Col>
          <Col span={11}>
            <b style={{ fontSize: '18px' }}>Quản Lý Dịch Vụ</b>
          </Col>
          <Col span={3} offset={3}>
            <Button
              type="primary"
              style={{ borderRadius: '4px', padding: '0px 7px' }}
              onClick={() => history.push(urlAdd)}
              disabled={(this.state.selectedRowKeys.length > 0)}
            >
              {window.innerWidth < 455 ? <Icon type="plus-circle" /> : 'Tạo dịch vụ '}
            </Button>
          </Col>
          <Col span={3}>
            <Button style={{ borderRadius: '4px', padding: '0px 7px' }}
              onClick={() => {
                this.updateStatusService();
              }}
              disabled={(this.state.selectedRowKeys.length > 0)}
            >
              {window.innerWidth < 455 ? <Icon type="check-circle" /> : 'Lưu'}
            </Button>
          </Col>
          <Col span={3}>
            <Button type="danger" style={{ borderRadius: '4px', padding: '0px 7px' }}
              onClick={() => {
                this.cancelService();
              }}
              disabled={(this.state.selectedRowKeys.length > 0)}
            >
              {window.innerWidth < 455 ? <Icon type="redo" /> : 'Hủy'}
            </Button>
          </Col>
        </Row>

        <Row type="flex">
          <Col>
            <div>
              {/* <Popover
                content={topic.map((item) => (
                  <div>
                    <Checkbox><span style={{ color: item.color }}>
                      {item.label}
                    </span></Checkbox>
                  </div>
                ))}
                title="Title"
                trigger="click"
                visible={this.state.checkAdd}
                onVisibleChange={this.handleCheckAdd}
              >
                <Button
                  type="primary"
                  style={{ borderRadius: '4px', padding: '0px 7px' }}
                  disabled={!(this.state.selectedRowKeys.length > 0)}
                >
                  {window.innerWidth < 455 ? <Icon type="plus-square" /> : 'Gán Chủ Đề'}
                </Button>
              </Popover>

              <Dropdown overlay={this.menuAdd}  disabled={!(this.state.selectedRowKeys.length > 0)}  trigger={['click']}>
                <Button
                  type="primary"
                  style={{ borderRadius: '4px', padding: '0px 7px' }}

                >
                  {window.innerWidth < 455 ? <Icon type="plus-square" /> : 'Gán Chủ Đề'}
                </Button>
              </Dropdown> */}
              <div>
                <Button
                  type="primary"
                  style={{ borderRadius: '4px', padding: '0px 7px' }}
                  onClick={() => {
                    const checkboxesAdd = document.getElementById("checkboxesAdd");
                    const buttonDel = document.getElementById("buttonDel");
                    if (!checkAdd) {
                      checkboxesAdd.classList.remove('topoic-select');
                      buttonDel.classList.add('topoic-button-del');
                      checkAdd = true;
                    } else {
                      checkboxesAdd.classList.add('topoic-select');
                      buttonDel.classList.remove('topoic-button-del');
                      checkAdd = false;
                    }
                  }
                  }
                  disabled={!(this.state.selectedRowKeys.length > 0)}
                  id="buttonAdd"
                >
                  {window.innerWidth < 455 ? <Icon type="plus-square" /> : 'Gán Chủ Đề'}
                </Button>
              </div>
              <div id="checkboxesAdd" className="topoic-select">
                <div>
                  <CheckboxGroup options={topic} onChange={this.onCheckAddChange} />
                  {/* {topic.map((item) => (
                    <div>
                      <Checkbox key = {item.key} onChange={(item) => this.onCheckAddChange(item)}><span style={{ color: item.color }}>
                        {item.label}
                      </span></Checkbox>
                    </div>
                  ))} */}
                </div>
                <div>
                  <Button style={{ border: 'none', color: 'darkorange', padding: '0px 7px' }} onClick={this.saveAddTopic}>Lưu</Button>
                  <Button style={{ border: 'none', color: 'red', padding: '0px 7px' }}>Hủy</Button>
                </div>
              </div>

            </div>
          </Col>
          <Col offset={1}>
            <div>
              <div>
                <Button
                  type="primary"
                  style={{ borderRadius: '4px', padding: '0px 7px' }}
                  onClick={() => {
                    const checkboxesAdd = document.getElementById("checkboxesDel");
                    const buttonAdd = document.getElementById("buttonAdd");
                    if (!checkDel) {
                      checkboxesAdd.classList.remove('topoic-select-del');
                      buttonAdd.classList.add('topoic-button-add');
                      checkDel = true;
                    } else {
                      checkboxesAdd.classList.add('topoic-select-del');
                      buttonAdd.classList.remove('topoic-button-add');
                      checkDel = false;
                    }
                  }
                  }
                  disabled={!(this.state.selectedRowKeys.length > 0)}
                  id="buttonDel"
                >
                  {window.innerWidth < 455 ? <Icon type="minus-square" /> : 'Bỏ Chủ Đề'}
                </Button>
              </div>
              <div id="checkboxesDel" className="topoic-select-del">
                <div>
                  <CheckboxGroup options={topic} onChange={this.onCheckDelChange} />
                </div>
                <div>
                  <Button style={{ border: 'none', color: 'darkorange', padding: '0px 7px' }} onClick={this.saveDelTopic}>Lưu</Button>
                  <Button style={{ border: 'none', color: 'red', padding: '0px 7px' }}>Hủy</Button>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </div >
    );

    const columns = [
      {
        title: 'Mã',
        dataIndex: 'id',
        key: 'id',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.id - b.id,
      },
      {
        title: 'Tên',
        dataIndex: 'name',
        key: 'name',
        ...this.getColumnSearchProps('name'),
      },
      {
        title: 'Thời Gian',
        dataIndex: 'estimation',
        key: 'estimation',
        sorter: (a, b) => a.estimation - b.estimation,
      },
      {
        title: 'Giá',
        dataIndex: 'price',
        key: 'price',
        sorter: (a, b) => a.price - b.price,
      },
      {
        title: 'Trạng Thái',
        dataIndex: 'status',
        key: 'status',
        width: 100,
        filters: [{
          text: 'Kích hoạt',
          value: 'active',
        }, {
          text: 'Khóa',
          value: 'inactive',
        }, {
          text: 'Chờ xử lý',
          value: 'pending',
        }, {
          text: 'Cấm',
          value: 'ban',
        }],

        onFilter: (value, record) => {
          const orderStatus = record.status.split('-');
          const value1 = orderStatus[0];
          return value1.indexOf(value) === 0;
        },

        render: (text) => {
          const orderStatus = text.split('-');
          return (
            <div style={{ width: '100%' }}>
              <Select
                defaultValue={text}
                style={{ width: '100%' }}
                onSelect={(e) => handleStatusChange(e)}
              >
                {status.map((item) => (
                  <Option
                    key={`${item.key}-${orderStatus[1]}`}
                    value={`${item.key}-${orderStatus[1]}`}
                  >
                    <p style={{ color: item.color }}>
                      {item.label}
                    </p>
                  </Option>
                ))}
              </Select>
            </div>
          );
        },
      },
      {
        title: 'Chủ Đề',
        dataIndex: 'topic',
        key: 'topic',
        width: 100,
        filters: [{
          text: 'Yêu Thích',
          value: 1,
        }, {
          text: 'Mùa Hè',
          value: 2,
        }, {
          text: 'Cá Tính',
          value: 4,
        }, {
          text: 'Mùa Xuân',
          value: 8,
        }],

        onFilter: (value, record) => {
          const value1 = parseInt(record.topic);
          return (value1 & value) === value;
        },

        render: (text) => {
          const listTopic = this.showTopic(parseInt(text));
          return (
            <div style={{ width: '100%' }}>
              <Select
                defaultValue={"Thuộc Về"}
                style={{ width: '100%' }}
              >
                {
                  listTopic.map(list => (
                    topic.map((item) => (
                      list === item.key ? (
                        <Option
                          key={`${item.key}`}
                          value={`${item.value}`}
                          disabled={true}
                        >
                          <p style={{ color: item.color }}>
                            {item.label}
                          </p>
                        </Option>
                      ) : null
                    ))
                  ))
                }
              </Select>
            </div>
          );
        },
      },
      {
        title: 'Mô Tả',
        dataIndex: 'description',
        key: 'description',
        ...this.getColumnSearchProps('description'),
      },
      {
        title: 'Thao Tác',
        fixed: 'right',
        width: 60,
        render: (text, record) => {
          return (
            <Row span={24}>
              <Col span={12}>
                <Link to={`${urlEdit}/${record.id}`}>
                  <Tooltip title="Sửa">
                    <Icon type="edit" />
                  </Tooltip>
                </Link>
              </Col>
              <Col span={12}>
                <Popconfirm title="Bạn muốn xóa?" onConfirm={() => this.handleDelete(record.id)}>
                  <Link to={'#'}>
                    <Tooltip title="Xóa">
                      <Icon type="delete" />
                    </Tooltip>
                  </Link>
                </Popconfirm>
              </Col>
            </Row>
          );
        },
      },
    ];

    return (
      <div >
        <Modal
          title="Thông Báo"
          visible={this.state.visibleModal}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          {this.props.services.message == 'Update service successfully' ? (
            <p>Cập nhật thành công!</p>
          ) : (
              <p>Cập nhật thất bại!</p>
            )}
        </Modal>
        <Table
          {...this.state}
          loading={services === undefined ? true : false}
          title={title}
          columns={columns}
          dataSource={data}
          pagination={{ pageSize: 10 }}
          rowSelection={rowSelection}
          scroll={{ x: true }}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { services } = state;
  return { services };
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({
    getServices,
    updateService,
    changeServiceStatus,
  },
    dispatch
  );

const connectedServiceTable = connect(
  mapStateToProps,
  mapDispatchToProps
)(ServiceTable);
export { connectedServiceTable as ServiceTable };
