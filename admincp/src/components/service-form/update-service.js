import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Select, Col, Form, Input, Button, Row, Modal, Spin, Empty } from 'antd';
import { updateService, getServicesById } from '../../redux/a_service/action';
import { history } from '../../redux/store';

const status = [
  { key: 1, value: 'active', label: 'Kích hoạt', color: '#00BB00' },
  { key: 2, value: 'inactive', label: 'Khóa', color: '#FF0000' },
  { key: 3, value: 'pending', label: 'Chờ xử lý', color: '#FFAA00' },
  { key: 4, value: 'ban', label: 'Cấm', color: '#FF0000' },
];
const Option = Select.Option;
const url = window.location.pathname.substr(8, 19);

class UpdateService extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      name: '',
      status: '',
      svid: '',
      visible: false,
    };
  }

  componentWillMount() {
    const key = window.location.pathname.split('/');
    const svid = key[key.length - 1];
    this.setState({ svid: svid });
    this.props.getServicesById(svid);
  }

  update = () => {
    const description = this._description.value;
    const price = this._price.state.value;
    const name = this._name.state.value;
    const estimation = this._estimation.state.value;
    const statusF = this._status.props.defaultValue;
    const statusS = this.state.status;
    const status = statusS !== '' ? statusS : statusF;
    const svid = this.state.svid;
    const service = { name, price, estimation, description, status };
    const data = { svid, service };
    this.setState({ visible: true, name: name });
    this.props.updateService(data);
  }

  getDisable = (e) => {
    this.setState({ status: e });
  }

  handleOk = (e) => {
    this.setState({
      visible: false,
    });
    history.push(url);
  }

  handleCancel = (e) => {
    this.setState({
      visible: false,
    });
  }

  render() {
    const service = this.props.services.service;
    let data = {};
    if (!service) {
      return (
        <div>
          <Spin>
            <Empty />
          </Spin>
        </div>
      )
    } else {
      data = service[0];
    }

    return (
      <div>
        <Modal
          title="Thông Báo"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          {this.props.services.message == 'Update service successfully' ? <p>Cập nhật ' {this.state.name} ' thành công!</p> : <p>Cập nhật ' {this.state.name} ' thất bại!</p>}
        </Modal>
        <h3>Sửa Dịch Vụ</h3>
        <Form>
          <Row>
            <Col xs={20} xl={10} offset={1}>
              <Form.Item label="Tên:">
                <Input type="text" ref={Input => this._name = Input} defaultValue={data === undefined ? '' : data['name']} />
              </Form.Item>
              <Form.Item label="Giá:">
                <Input type="text" ref={Input => this._price = Input} defaultValue={data === undefined ? '' : data['price']} />
              </Form.Item>
            </Col>


            <Col xs={20} xl={10} offset={1}>
              <Form.Item label="Thời gian:">
                <Input type="text" ref={Input => this._estimation = Input} defaultValue={data === undefined ? '' : data['estimation']} />
              </Form.Item>
              <Form.Item label="Trạng thái:">
                <Select
                  defaultValue={data.status}
                  ref={(Select) => (this._status = Select)}
                  onSelect={this.getDisable}
                >
                  {
                    status.map((i) => (
                      <Option key={i.key} value={i.value}>
                        <p style={{ color: i.color }}>
                          {i.label}
                        </p>
                      </Option>
                    ))
                  }
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={22} offset={1}>
              <Form.Item label="Mô tả:">
                <textarea style={{ width: '96%' }} ref={textarea => this._description = textarea} defaultValue={data === undefined ? '' : data['description']}>
                </textarea>
              </Form.Item>
            </Col>
          </Row>
        </Form>

        <Col span={22} style={{ marginTop: '5%' }}>
          <Row type="flex" justify="center">
            <Col offset={2}>
              <Button size="default" type="primary" style={{ borderRadius: '4px' }}
                onClick={() => { this.update() }}>
                Lưu
              </Button>
            </Col>
            <Col offset={2}>
              <Button size="default" type="danger" style={{ borderRadius: '4px' }}
                onClick={() => history.push(url)}>
                Hủy
              </Button>
            </Col>
          </Row>
        </Col>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { services } = state;
  return { services };
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      updateService,
      getServicesById,
    },
    dispatch
  );

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdateService);
export { connected as UpdateService };
