import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getOrder, changeOrderStatus } from '../../redux/order/action';
import { Table, Row, Col, Input, Button, Icon, Select, Spin, Empty, Modal } from 'antd';
import './order-table.css';
import Highlighter from 'react-highlight-words';
import { history } from '../../redux/store';

const Option = Select.Option;
let statusChange = [];
const status = [
  { key: 'pending', value: 1, label: 'Chờ xử lý', color: '#FFAA00' },
  { key: 'accept', value: 2, label: 'Chấp nhận', color: '#0000FF' },
  { key: 'reject', value: 3, label: 'Hủy bỏ', color: '#FF0000' },
  { key: 'finish', value: 4, label: 'Hoàn thành', color: '#00BB00' },
];

const handleStatusChange = (e) => {
  const change = e.split('-');
  const Cstatus = status.filter((option) => option.key === change[0]);
  const value = Cstatus[0].value;
  statusChange[change[1]] = {
    status: value,
    id: change[1],
  };
};

const expandedRowRender = (record) => {
  const columns = [
    { title: 'Mã', dataIndex: 'osvid', key: 'osvid' },
    { title: 'Thợ', dataIndex: 'worker', key: 'worker' },
    { title: 'Dịch vụ', dataIndex: 'service', key: 'service' },
    { title: 'Giá', dataIndex: 'price', key: 'price' },
    { title: 'Thời gian', dataIndex: 'time', key: 'time' },
    { title: 'Đánh giá', dataIndex: 'reviewStar', key: 'reviewStar' },
    { title: 'Phản hồi', dataIndex: 'reviewContent', key: 'reviewContent' },
  ];
  const title = () => <b>Chi tiết đơn hàng </b>;
  return (
    <Table
      title={title}
      bordered={true}
      columns={columns}
      dataSource={record.orderService}
      pagination={false}
    />
  );
};

class OrderTable extends React.Component {
  state = {
    searchText: '',
    bordered: true,
    loading: false,
    size: 'middle',
    rowSelection: {},
    expandedRowRender,
    visibleModal: false,
  };

  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys, selectedKeys, confirm, clearFilters,
    }) => (
        <div style={{ padding: 8 }}>
          <Input
            ref={node => { this.searchInput = node; }}
            placeholder={`Tìm ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
            onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
            style={{ width: 188, marginBottom: 8, display: 'block' }}
          />
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm)}
            icon="search"
            size="small"
            style={{ width: 90, marginRight: 8, borderRadius: '4px' }}
          >
            Search
        </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90, borderRadius: '4px' }}
          >
            Reset
        </Button>
        </div>
      ),
    filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) => (
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    ),
  })

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: '' });
  }

  componentWillMount() {
    this.props.getOrder();
  }

  handleOk = (e) => {
    this.setState({
      visibleModal: false,
    });
    this.props.getOrder();
  };

  handleCancel = (e) => {
    this.setState({
      visibleModal: false,
    });
    this.props.getOrder();
  };

  updateOrder = () => {
    statusChange = statusChange.filter(function (n) { return n != undefined });
    this.props.changeOrderStatus(statusChange);
    this.setState({ visibleModal: true });
  }

  cancelOrder = () => {
    window.location.reload();
  }

  render() {
    let data = [];
    const { orders } = this.props.order;
    if (orders) {
      orders.map((order, index) => {
        data.push({
          id: `${order.oid}`,
          key: `${order.oid}`,
          client: `${order.client}`,
          use: `${order.use}`,
          time: `${order.time}`,
          price: `${order.price}`,
          status: order.status,
          note: order.note,
          issue: order.issue,
          accept: order.accept,
          orderService: order.orderService,
        });
      });
    } else {
      return (
        <div>
          <Spin>
            <Empty />
          </Spin>
        </div>
      );
    }

    const title = () => (
      <Row type="flex">
        <Col span={1}></Col>
        <Col span={11}>
          <b style={{ fontSize: '18px' }}>Quản Lý Đặt Hàng</b>
        </Col>
        <Col span={3} offset={3}>
          <Button type="primary" style={{ borderRadius: '4px', padding: '0px 7px' }}>
            {window.innerWidth < 455 ? <Icon type="plus-circle" /> : 'Thêm'}
          </Button>
        </Col>
        <Col span={3}>
          <Button
            style={{ backgroundColor: '#FFAA00', borderRadius: '4px', padding: '0px 7px' }}
            onClick={() => {
              this.updateOrder();
            }}
          >
            {window.innerWidth < 455 ? <Icon type="check-circle" /> : 'Lưu'}
          </Button>
        </Col>
        <Col span={3}>
          <Button type="danger" style={{ borderRadius: '4px', padding: '0px 7px' }}
            onClick={() => {
              this.cancelOrder();
            }}
          >
            {window.innerWidth < 455 ? <Icon type="close-circle" /> : 'Hủy'}
          </Button>
        </Col>
      </Row>
    );

    const columns = [
      {
        title: 'Mã',
        dataIndex: 'id',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.id - b.id,
      },
      {
        title: 'Người Đặt',
        dataIndex: 'client',
        ...this.getColumnSearchProps('client'),
      },
      {
        title: 'Người Dùng',
        dataIndex: 'use',
        ...this.getColumnSearchProps('use'),
      },
      {
        title: 'Bắt Đầu',
        dataIndex: 'time',
        defaultSortOrder: 'descend',
        width: 100,
        sorter: (a, b) => a.time - b.time,
      },
      {
        title: 'Tổng Tiền',
        dataIndex: 'price',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.price - b.price,
      },
      {
        title: 'Trạng Thái',
        dataIndex: 'status',
        key: 'status',
        width: 100,
        filters: [{
          text: 'Chờ xử lý',
          value: 'pending',
        }, {
          text: 'Chấp nhận',
          value: 'accept',
        }, {
          text: 'Hủy bỏ',
          value: 'reject',
        }, {
          text: 'Hoàn thành',
          value: 'finish',
        }],

        onFilter: (value, record) => {
          const orderStatus = record.status.split('-');
          const Cstatus = status.filter((option) => option.key === orderStatus[0]);
          const value1 = Cstatus[0].key;
          return value1.indexOf(value) === 0;
        },

        render: (text) => {
          const orderStatus = text.split('-');
          const Cstatus = status.filter((option) => option.key === orderStatus[0]);
          const value = Cstatus[0].value;
          return (
            <div style={{ width: '100%' }}>
              <div
                className="order-status-hidden"
                id={`order-status-input-${orderStatus[1]}`}
              >
                <Input
                  prefix={
                    <Icon
                      type="left"
                      onClick={() => {
                        const select = document.getElementById(
                          `order-status-select-${orderStatus[1]}`
                        );
                        const input = document.getElementById(
                          `order-status-input-${orderStatus[1]}`
                        );
                        input.classList.add('order-status-hidden');
                        select.classList.remove('order-status-hidden');
                      }}
                    />
                  }
                  type="text"
                  onChange={e => {
                    statusChange[orderStatus[1]] = {
                      ...statusChange[orderStatus[1]], message: e.target.value, id: orderStatus[1],
                      status: (statusChange[orderStatus[1]] !== undefined ? statusChange[orderStatus[1]].status : value)
                    }
                  }}
                />
              </div>
              <div id={`order-status-select-${orderStatus[1]}`}>
                <Select
                  defaultValue={text}
                  style={{ width: '100%' }}
                  onSelect={(e) => handleStatusChange(e)}
                >
                  {status.map((item) => (
                    <Option
                      key={`${item.key}-${orderStatus[1]}`}
                      value={`${item.key}-${orderStatus[1]}`}
                    >
                      <p
                        style={{ color: item.color }}
                        onClick={() => {
                          const select = document.getElementById(
                            `order-status-select-${orderStatus[1]}`
                          );
                          const input = document.getElementById(
                            `order-status-input-${orderStatus[1]}`
                          );
                          select.classList.add('order-status-hidden');
                          input.classList.remove('order-status-hidden');
                        }}
                      >
                        {item.label}
                      </p>
                    </Option>
                  ))}
                </Select>
              </div>
            </div>
          );
        },
      },
      {
        title: 'Ghi Chú',
        dataIndex: 'note',
      },
      {
        title: '?Hủy Bỏ',
        dataIndex: 'issue',
        key: 'issue',
      },
      {
        title: '?Chấp Nhận',
        dataIndex: 'accept',
      },
    ];
    return (
      <div>
        <Modal
          title="Thông Báo"
          visible={this.state.visibleModal}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          {this.props.order.message == 'Update status successfully' ? (
            <p>Cập nhật dịch vụ thành công!</p>
          ) : (
              <p>Cập nhật dịch vụ thất bại!</p>
            )}
        </Modal>
        <Table
          {...this.state}
          loading={orders === undefined ? true : false}
          columns={columns}
          title={title}
          dataSource={data}
          pagination={{ pageSize: 10 }}
          scroll={{ x: true }}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { order } = state;
  return { order };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getOrder,
      changeOrderStatus,
    },
    dispatch
  );

const connectedOrderTable = connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderTable);
export { connectedOrderTable as OrderTable };