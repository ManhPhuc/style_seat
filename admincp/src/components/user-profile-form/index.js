import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUsers, changeUserStatus } from '../../redux/user/action';
import { Table, Row, Col, Input, Button, Icon, Select, Spin, Empty, Tooltip, Modal, Popconfirm } from 'antd';
import { Link } from 'react-router-dom';
import Highlighter from 'react-highlight-words';
import { history } from '../../redux/store';

const Option = Select.Option;
let selectChange = [];
const status = [
  { key: 'active', value: 1, label: 'Kích hoạt', color: '#00BB00' },
  { key: 'inactive', value: 2, label: 'Khóa', color: '#FF0000' },
  { key: 'pending', value: 3, label: 'Chờ xử lý', color: '#FFAA00' },
  { key: 'ban', value: 4, label: 'Cấm', color: '#FF0000' },
];
const type = [
  { key: 'ROLE_GENERALMANAGER', value: 1, label: 'Quản trị', color: '#BC0900' },
  { key: 'ROLE_MANAGER', value: 2, label: 'Quản lý', color: '#DF1000' },
  { key: 'ROLE_WORKER', value: 3, label: 'Thợ', color: '#FF4A00' },
  { key: 'ROLE_CLIENT', value: 4, label: 'Khách', color: '#FF7F00' },
];

const handleSelectChange = (e) => {
  const change = e.split('-');
  if (change[2] == 0) {
    let Cstatus = status.filter((option) => option.key === change[0]);
    let value = Cstatus[0].value;
    selectChange[change[1]] = { ...selectChange[change[1]], status: value, id: change[1] }
  } else if (change[2] == 1) {
    let Ctype = type.filter((option) => option.key === change[0]);
    let value = Ctype[0].value;
    selectChange[change[1]] = { ...selectChange[change[1]], type: value, id: change[1] }
  }
};
const urlEdit = '/dashboard/user/user-profile';
const urlAdd = '/dashboard/user/add-user'

class UserTable extends Component {
  state = {
    searchText: '',
    bordered: true,
    loading: false,
    size: 'middle',
    rowSelection: {},
    visibleModal: false,
  };

  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys, selectedKeys, confirm, clearFilters,
    }) => (
        <div style={{ padding: 8 }}>
          <Input
            ref={node => { this.searchInput = node; }}
            placeholder={`Tìm ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
            onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
            style={{ width: 188, marginBottom: 8, display: 'block' }}
          />
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm)}
            icon="search"
            size="small"
            style={{ width: 90, marginRight: 8, borderRadius: '4px' }}
          >
            Tìm
        </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90, borderRadius: '4px' }}
          >
            Hủy
        </Button>
        </div>
      ),
    filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) => (
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    ),
  })

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: '' });
  }

  componentWillMount() {
    this.props.getUsers();
  }

  handleOk = (e) => {
    this.setState({
      visibleModal: false,
    });
    this.props.getUsers();
  };

  handleCancel = (e) => {
    this.setState({
      visibleModal: false,
    });
    this.props.getUsers();
  };

  updateUser = () => {
    selectChange = selectChange.filter(function (n) { return n != undefined });
    this.props.changeUserStatus(selectChange);
    this.setState({ visibleModal: true });
  }

  cancelUser = () => {
    window.location.reload();
  }

  handleDelete = (id) => {

  }

  render() {
    let data = [];
    const users = this.props.users.users;
    if (users) {
      users.map((user, index) => {
        data.push({
          id: `${user.uid}`,
          key: index,
          email: `${user.email}`,
          phone: `${user.phone}`,
          name: user.firstName ? `${user.firstName}` + ' ' + `${user.lastName}` : '',
          status: `${user.status}` + '-' + `${user.uid}` + '-0',
          type: `${user.type}` + '-' + `${user.uid}` + '-1',
          address: `${user.address}`,
        });
      });
    } else {
      return (
        <div>
          <Spin>
            <Empty />
          </Spin>
        </div>
      );
    }

    const title = () => (
      <Row type="flex" >
        <Col span={1}></Col>
        <Col span={11}>
          <b style={{ fontSize: '18px' }}>Quản Lý Tài Khoản</b>
        </Col>
        <Col span={3} offset={3}>
          <Button
            type="primary"
            style={{ borderRadius: '4px', padding: '0px 7px' }}
            onClick={() => history.push(urlAdd)}
          >
            {window.innerWidth < 455 ? <Icon type="plus-circle" /> : 'Tạo tài khoản '}
          </Button>
        </Col>
        <Col span={3}>
          <Button style={{ backgroundColor: '#FFAA00', borderRadius: '4px', padding: '0px 7px' }}
            onClick={() => {
              this.updateUser();
            }}
          >
            {window.innerWidth < 455 ? <Icon type="check-circle" /> : 'Lưu'}
          </Button>
        </Col>
        <Col span={3}>
          <Button type="danger" style={{ borderRadius: '4px', padding: '0px 7px' }}
            onClick={() => {
              this.cancelUser();
            }}
          >
            {window.innerWidth < 455 ? <Icon type="close-circle" /> : 'Hủy'}
          </Button>
        </Col>
      </Row>
    );

    const columns = [
      {
        title: 'Mã',
        dataIndex: 'id',
        key: 'id',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.id - b.id,
      },
      {
        title: 'Gmail',
        dataIndex: 'email',
        key: 'email',
        ...this.getColumnSearchProps('email'),
      },
      {
        title: 'SĐT',
        dataIndex: 'phone',
        key: 'phone',
        ...this.getColumnSearchProps('phone'),
      },
      {
        title: 'Tên',
        dataIndex: 'name',
        key: 'name',
        ...this.getColumnSearchProps('name'),
      },
      {
        title: 'Trạng Thái',
        dataIndex: 'status',
        key: 'status',
        width: 100,
        filters: [{
          text: 'Kích hoạt',
          value: 'active',
        }, {
          text: 'Khóa',
          value: 'inactive',
        }, {
          text: 'Chờ xử lý',
          value: 'pending',
        }, {
          text: 'Cấm',
          value: 'ban',
        }],

        onFilter: (value, record) => {
          const orderStatus = record.status.split('-');
          const value1 = orderStatus[0];
          return value1.indexOf(value) === 0;
        },

        render: (text) => {
          const orderStatus = text.split('-');
          return (
            <div style={{ width: '100%' }}>
              <Select
                defaultValue={text}
                style={{ width: '100%' }}
                onSelect={(e) => handleSelectChange(e)}
              >
                {status.map((item) => (
                  <Option
                    key={`${item.key}-${orderStatus[1]}-0`}
                    value={`${item.key}-${orderStatus[1]}-0`}
                  >
                    <p style={{ color: item.color }}>
                      {item.label}
                    </p>
                  </Option>
                ))}
              </Select>
            </div>
          );
        },
      },
      {
        title: 'Loại',
        dataIndex: 'type',
        key: 'type',
        width: 100,
        filters: [{
          text: 'Quản trị',
          value: 'ROLE_GENERALMANAGER',
        }, {
          text: 'Quản lý',
          value: 'ROLE_MANAGER',
        }, {
          text: 'Thợ',
          value: 'ROLE_WORKER',
        }, {
          text: 'Khách',
          value: 'ROLE_CLIENT',
        }],

        onFilter: (value, record) => {
          const orderType = record.type.split('-');
          const value1 = orderType[0];
          return value1.indexOf(value) === 0;
        },

        render: (text) => {
          const orderType = text.split('-');
          return (
            <div style={{ width: '100%' }}>
              <Select
                defaultValue={text}
                style={{ width: '100%' }}
                onSelect={(e) => handleSelectChange(e)}
              >
                {type.map((item) => (
                  <Option
                    key={`${item.key}-${orderType[1]}-1`}
                    value={`${item.key}-${orderType[1]}-1`}
                  >
                    <p style={{ color: item.color }}>
                      {item.label}
                    </p>
                  </Option>
                ))}
              </Select>
            </div>
          );
        },
      },
      {
        title: 'Thao Tác',
        fixed: 'right',
        width: 60,
        render: (text, record) => {
          return (
            <Row span={24}>
              <Col span={12}>
                <Link to={`${urlEdit}/${record.id}`}>
                  <Tooltip title="Sửa">
                    <Icon type="edit" />
                  </Tooltip>
                </Link>
              </Col>
              <Col span={12}>
                <Popconfirm title="Bạn muốn xóa?" onConfirm={() => this.handleDelete(record.id)}>
                  <Link to={'#'}>
                    <Tooltip title="Xóa">
                      <Icon type="delete" />
                    </Tooltip>
                  </Link>
                </Popconfirm>
              </Col>
            </Row>
          );
        },
      },
    ];

    return (
      <div >
        <Modal
          title="Thông Báo"
          visible={this.state.visibleModal}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          {this.props.users.message == 'Update user successfully' ? (
            <p>Cập nhật thành công!</p>
          ) : (
              <p>Cập nhật thất bại!</p>
            )}
        </Modal>
        <Table
          {...this.state}
          loading={users === undefined ? true : false}
          title={title}
          columns={columns}
          dataSource={data}
          pagination={{ pageSize: 10 }}
          scroll={{ x: true }}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { users } = state;
  return { users };
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({
    getUsers,
    changeUserStatus,
  },
    dispatch
  );

const connectedUserTable = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserTable);
export { connectedUserTable as UserTable };
