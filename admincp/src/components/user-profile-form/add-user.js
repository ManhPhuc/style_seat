import React, { Component, isValidElement } from 'react';
import { bindActionCreators } from 'redux';
import './user-profile-form.css';
import { connect } from 'react-redux';
import { Select, Col, Form, Input, Button, Row, Modal } from 'antd';
import { Password } from '../password';
import { addUser, getUsers } from '../../redux/user/action';
import { history } from '../../redux/store';

const Option = Select.Option;
const status = [
  { key: 1, value: 'active', label: 'Kích hoạt', color: '#00BB00' },
  { key: 2, value: 'inactive' , label: 'Khóa', color: '#FF0000' },
  { key: 3, value: 'pending', label: 'Chờ xử lý', color: '#FFAA00' },
  { key: 4, value: 'ban', label: 'Cấm', color: '#FF0000' },
];
const type = [
  { key: 1 , value: 'ROLE_GENERALMANAGER', label: 'Quản trị', color: '#BC0900' },
  { key: 2 , value: 'ROLE_MANAGER', label: 'Quản lý', color: '#DF1000' },
  { key: 3 , value: 'ROLE_WORKER', label: 'Thợ', color: '#FF4A00' },
  { key: 4 , value: 'ROLE_CLIENT', label: 'Khách', color: '#FF7F00' },
];
const isDisable = [
  { key: 1, label: 'Kích hoạt', color: '#00BB00' },
  { key: 0, label: 'Hủy bỏ', color: '#FF0000' },
];
let url = window.location.pathname;
let url2 = url.replace('/add-user', '');
const url3 = url2.replace('/admincp', '');

class AddUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDisable: '',
      type: '',
      status: '',
      visible: false,
      email: '',
    };
  }

  getisDisable = (e) => {
    this.setState({ isDisable: e });
  }

  getType = (e) => {
    this.setState({ type: e });
  }

  getStatus = (e) => {
    this.setState({ status: e });
  }

  createUser = () => {
    const email = this._email.state.value;
    const phone = this._phone.state.value;
    const password = this._password.state.value;
    const type = this.state.type;
    const status = this.state.status;
    const isDisable = this.state.isDisable;
    const data = { email, phone, password, type, status, isDisable };
    this.props.addUser(data);
    this.setState({ visible: true, email: email });
  }

  handleOk = (e) => {
    this.setState({
      visible: false,
    });
    this.props.getUsers();
    history.push(url3);
  }

  handleCancel = (e) => {
    this.setState({
      visible: false,
    });
  }
  render() {
    return (
      <div>
        <Modal
          title="Thông Báo"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          {this.props.users.message == 'Created user successfully' ? <p>Thêm ' {this.state.email} ' thành công!</p> : <p>Thêm ' {this.state.email} ' thất bại!</p>}
        </Modal>

        <h3>Thêm Tài Khoản</h3>
        <Form>
          <Row>
            <Col xs={20} xl={10} offset={1}>
              <Form.Item label="Email:">
                <Input
                  type="email"
                  ref={(Input) => (this._email = Input)}
                />
              </Form.Item>
              <Form.Item label="Mật khẩu:">
                <Input
                  type="text"
                  ref={(Input) => (this._password = Input)}
                />
              </Form.Item>
              {/* <Form.Item label="Password:">
                <Input
                  type="password"
                  addonAfter={
                    <a
                      className="col-1"
                      data-toggle="collapse"
                      href="#passwordCollapse"
                      role="button"
                      aria-expanded="false"
                      aria-controls="passwordCollapse"
                    >
                      <i className="fas fa-edit" />
                    </a>
                  }
                  disabled
                />
                <Password />
              </Form.Item> */}
              <Form.Item label="Vô hiệu hóa:">
                <Select
                  onSelect={this.getisDisable}
                >
                  {isDisable.map((i) => (
                    <Option key={i.key} value={i.key}>
                      <p style={{ color: i.color }}>
                        {i.label}
                      </p>
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col xs={20} xl={10} offset={1}>
              <Form.Item label="Số điện thoại: ">
                <Input
                  type="phone"
                  ref={(Input) => (this._phone = Input)}
                />
              </Form.Item>
              <Form.Item label="Loại:">
                <Select
                  onSelect={this.getType}
                >
                  {type.map((i) => (
                    <Option key={i.key} value={i.value}>
                      <p style={{ color: i.color }}>
                        {i.label}
                      </p>
                    </Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item label="Trạng thái:">
                <Select
                  onSelect={this.getStatus}
                >
                  {status.map((i) => (
                    <Option key={i.key} value={i.value}>
                      <p style={{ color: i.color }}>
                        {i.label}
                      </p>
                    </Option>
                  ))}
                </Select>
              </Form.Item>

            </Col>
          </Row>
        </Form>
        <Col span={22} style={{ marginTop: '5%' }}>
          <Row type="flex" justify="center">
            <Col offset={2}>
              <Button size="default" type="primary" style={{ borderRadius: '4px' }}
                onClick={() => { this.createUser() }}
              >
                Lưu
            </Button>
            </Col>
            <Col offset={2}>
              <Button size="default" type="danger" style={{ borderRadius: '4px' }}
                onClick={() => history.push(url3)}>
                Hủy
            </Button>
            </Col>
          </Row>
        </Col>
      </div >
    );
  }
}

function mapStateToProps(state) {
  const { users } = state;
  return { users };
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      addUser,
      getUsers,
    },
    dispatch
  );

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddUser);
export { connected as AddUser };
