import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import './user-profile-form.css';
import { connect } from 'react-redux';
import { Select, Col, Form, Input, Button, Row, Modal, Spin, Empty } from 'antd';
import { Address } from '../address';
import { Password } from '../password';
import { getUsers, updateUsers, receiveUpdateUsers, getUserById } from '../../redux/user/action';
import { history } from '../../redux/store';

const Option = Select.Option;
const status = [
  { key: 1, value: 'active', label: 'Kích hoạt', color: '#00BB00' },
  { key: 2, value: 'inactive', label: 'Khóa', color: '#FF0000' },
  { key: 3, value: 'pending', label: 'Chờ xử lý', color: '#FFAA00' },
  { key: 4, value: 'ban', label: 'Cấm', color: '#FF0000' },
];
const type = [
  { key: 1, value: 'ROLE_GENERALMANAGER', label: 'Quản trị', color: '#BC0900' },
  { key: 2, value: 'ROLE_MANAGER', label: 'Quản lý', color: '#DF1000' },
  { key: 3, value: 'ROLE_WORKER', label: 'Thợ', color: '#FF4A00' },
  { key: 4, value: 'ROLE_CLIENT', label: 'Khách', color: '#FF7F00' },
];
const isDisable = [
  { key: 1, label: 'Kích hoạt', color: '#00BB00' },
  { key: 0, label: 'Hủy bỏ', color: '#FF0000' },
];
const gender = [{ key: 0, label: 'Nam' }, { key: 1, label: 'Nữ' }];
const url = window.location.pathname.substr(8, 15);

class UserProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uid: '',
      gender: '',
      type: '',
      status: '',
      isDisable: '',
      visible: false,
      email: '',
    };
  }

  componentWillMount() {
    const key = window.location.pathname.split('/');
    const uid = key[key.length - 1];
    this.setState({ uid: uid });
    this.props.getUserById(uid);
    console.log('url : ' + url);
  }

  getGender = (e) => {
    this.setState({ gender: e });
  };

  getType = (e) => {
    this.setState({ type: e });
  };

  getStatus = (e) => {
    this.setState({ status: e });
  };

  getisDisable = (e) => {
    this.setState({ isDisable: e });
  }

  updateUser = () => {
    const firstName = this._firstName.state.value;
    const lastName = this._lastName.state.value;
    const birthDay = this._birthDay.state.value;
    const genderF = this._gender.props.defaultValue;
    const genderS = this.state.gender;
    const ctid = 1;
    const address = 'Hà Nội';

    const email = this._email.state.value;
    const phone = this._phone.state.value;
    const typeF = this._type.props.defaultValue;
    const typeS = this.state.type;
    const statusF = this._status.props.defaultValue;
    const statusS = this.state.status;
    const isDisableF = this._isDisable.props.defaultValue;
    const isDisableS = this.state.isDisable;

    const gender = genderS !== '' ? genderS : genderF;
    const type = typeS !== '' ? typeS : typeF;
    const status = statusS !== '' ? statusS : statusF;
    const isDisable = isDisableS !== '' ? isDisableS : isDisableF;
    const uid = this.state.uid;

    const profile = { firstName, lastName, birthDay, gender, ctid, address };
    const user = { email, phone, type, status };
    const data = { uid, profile, user };

    this.setState({ visible: true, email: email });
    this.props.updateUsers(data);
    // this.props.getUsers();
  }

  handleOk = (e) => {
    this.setState({
      visible: false,
    });
    history.push(url);
  };

  handleCancel = (e) => {
    this.setState({
      visible: false,
    });
  };
  render() {
    const user = this.props.users.user;
    let data = {};
    // if(user){
    //   data = user;
    // }
    if (user === undefined) {
      return (
        <div>
          <Spin>
            <Empty />
          </Spin>
        </div>
      )
    } else {
      data = user;
    }
    return (
      <div>
        <Modal
          title="Thông Báo"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          {this.props.users.message == 'Update user successfully' ? (
            <p>Cập nhật ' {this.state.email} ' thành công!</p>
          ) : (
              <p>Cập nhật ' {this.state.email} ' thất bại!</p>
            )}
        </Modal>
        <h3>Sửa Tài Khoản</h3>
        <Form>
          <Col xs={20} xl={10} offset={1}>
            <Form.Item label="Họ:">
              <Input
                type="text"
                ref={(Input) => (this._firstName = Input)}
                defaultValue={data === undefined ? '' : data['firstName']}
              />
            </Form.Item>
            <Form.Item label="Tên:">
              <Input
                type="text"
                ref={(Input) => (this._lastName = Input)}
                defaultValue={data === undefined ? '' : data['lastName']}
              />
            </Form.Item>
            <Form.Item label="Ngày Sinh:">
              <Input
                type="date"
                ref={(Input) => (this._birthDay = Input)}
                defaultValue={data === undefined ? '' : data['birthDay']}
              />
            </Form.Item>
            <Form.Item label="Giới Tính:">
              <Select
                defaultValue={data['gender']}
                ref={(Select) => (this._gender = Select)}
                onSelect={this.getGender}
              >
                {gender.map((i) => (
                  <Option key={i.key} value={i.key}>
                    {i.label}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item label="Địa Chỉ:">
              <Input
                type="address"
                addonAfter={
                  <a
                    className="col-1"
                    data-toggle="collapse"
                    href="#addressCollapse"
                    role="button"
                    aria-expanded="false"
                    aria-controls="addressCollapse"
                  >
                    <i className="fas fa-edit" />
                  </a>
                }
                disabled
              />
              <Address />
            </Form.Item>
          </Col>
          <Col xs={20} xl={10} offset={2}>
            <Form.Item label="Email:">
              <Input
                type="email"
                ref={(Input) => (this._email = Input)}
                defaultValue={data === undefined ? '' : data['email']}
              />
            </Form.Item>
            <Form.Item label="Số Điện Thoại: ">
              <Input
                type="phone"
                ref={(Input) => (this._phone = Input)}
                defaultValue={data === undefined ? '' : data['phone']}
              />
            </Form.Item>
            <Form.Item label="Loại Tài Khoản:">
              <Select
                defaultValue={data['type']}
                ref={(Select) => (this._type = Select)}
                onSelect={this.getType}
              >
                {type.map((i) => (
                  <Option key={i.key} value={i.value}>
                    <span style={{ color: i.color }}>
                      {i.label}
                    </span>
                  </Option>
                ))}
              </Select>
            </Form.Item>

            <Form.Item label="Trạng Thái:">
              <Select
                defaultValue={data['status']}
                ref={(Select) => (this._status = Select)}
                onSelect={this.getStatus}
              >
                {status.map((i) => (
                  <Option key={i.key} value={i.value}>
                    <span style={{ color: i.color }}>
                      {i.label}
                    </span>
                  </Option>
                ))}
              </Select>
            </Form.Item>

            <Form.Item label="Vô hiệu hóa:">
              <Select
                defaultValue={data['isDisable']}
                ref={(Select) => (this._isDisable = Select)}
                onSelect={this.getisDisable}
              >
                {isDisable.map((i) => (
                  <Option key={i.key} value={i.key}>
                    <span style={{ color: i.color }}>
                      {i.label}
                    </span>
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item label="Mật Khẩu:">
              <Input
                type="password"
                addonAfter={
                  <a
                    className="col-1"
                    data-toggle="collapse"
                    href="#passwordCollapse"
                    role="button"
                    aria-expanded="false"
                    aria-controls="passwordCollapse"
                  >
                    <i className="fas fa-edit" />
                  </a>
                }
                disabled
              />
              <Password />
            </Form.Item>
          </Col>
        </Form>
        <Col span={22} style={{ marginTop: '5%' }}>
          <Row type="flex" justify="center">
            <Col offset={2}>
              <Button
                size="default"
                type="primary"
                style={{ borderRadius: '4px' }}
                onClick={() => {
                  this.updateUser();
                }}
              >
                Lưu
              </Button>
            </Col>
            <Col offset={2}>
              <Button
                size="default"
                type="danger"
                style={{ borderRadius: '4px' }}
                onClick={() => history.push(url)}
              >
                Hủy
              </Button>
            </Col>
          </Row>
        </Col>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { users } = state;
  return { users };
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getUsers,
      updateUsers,
      receiveUpdateUsers,
      getUserById,
    },
    dispatch
  );

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserProfile);
export { connected as UserProfile };
