import React from 'react';

export const PROFILE_HEADER = (url) => (
  <div>
    <div className="col-md-12">
      <div className="justify-content-start row mt-5">
        <div className="col-4">
          <img
            className="rounded-circle img-responsive"
            alt="Bootstrap Media Preview"
            src={
              url !== undefined
                ? url.url
                : 'https://images.pexels.com/photos/459947/pexels-photo-459947.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
            }
            style={{ width: '100px', height: '100px', float: 'right' }}
          />
        </div>
        <div className="col-8">
          <h3 className="mt-3">Worker's name</h3>
          <h6>Hair cut</h6>
        </div>
      </div>
    </div>
  </div>
);
