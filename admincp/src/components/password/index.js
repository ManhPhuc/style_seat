import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Collapse } from 'antd';
import './address.css';
// import {
//   getCountries,
//   getStates,
//   getCities,
//   selectCity,
// } from '../../redux/actions';

let countries,
  states,
  cities = [];

const Panel = Collapse.Panel;

class Password extends Component {
  constructor() {
    super();

    this.state = {
      citySelected: '',
    };

    // this.handleCountryChange = this.handleCountryChange.bind(this);
    // this.handleStateChange = this.handleStateChange.bind(this);
    // this.handleCityChange = this.handleCityChange.bind(this);
  }

  // componentWillMount() {
  //   this.props.getCountries();
  // }

  handleCountryChange(evt) {
    // this.props.getStates(evt.value);
  }

  handleStateChange(e) {
    // this.props.getCities(e.value);
  }

  handleCityChange(e) {
    // this.props.selectCity(e.value, this._address.value);
  }

  render() {
    const getaddress = undefined;
    const profile = undefined;
    // const { getaddress, profile } = this.props;
    // if (getaddress.country !== undefined) {
    //   countries = [];
    //   getaddress.country.map((country) =>
    //     countries.push({
    //       value: country.cid,
    //       label: country.name,
    //     })
    //   );
    // }
    return (
      <div>
        <div className="collapse" id="passwordCollapse">
          <div className="form-group row">
            <label className="col-sm-3 offset-sm-1 col-form-label">
              <div className="pull-left">Mật Khẩu Cũ *</div>
            </label>
            <input
              type="text"
              className="form-control col-sm-6"
              // defaultValue={profile.profile['address']}
              ref={(input) => (this._address = input)}
            />
          </div>
          <div className="form-group row">
            <label className="col-sm-3 offset-sm-1 col-form-label">
              <div className="pull-left">Mật Khẩu Mới *</div>
            </label>
            <input
              type="text"
              className="form-control col-sm-6"
              // defaultValue={profile.profile['address']}
              ref={(input) => (this._address = input)}
            />
          </div>
          <div className="form-group row">
            <label className="col-sm-3 offset-sm-1 col-form-label">
              <div className="pull-left">Nhập Lại *</div>
            </label>
            <input
              type="text"
              className="form-control col-sm-6"
              // defaultValue={profile.profile['address']}
              ref={(input) => (this._address = input)}
            />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { getaddress, profile } = state;
  return { getaddress, profile };
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      // getCountries,
      // getStates,
      // getCities,
      // selectCity,
    },
    dispatch
  );

const connectedCountry = connect(
  mapStateToProps,
  mapDispatchToProps
)(Password);
export { connectedCountry as Password };
