import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import './address.css';
import { Select } from 'antd';
// import {
//   getCountries,
//   getStates,
//   getCities,
//   selectCity,
// } from '../../redux/actions';

let countries,
  states,
  cities = [];

class Address extends Component {
  constructor() {
    super();

    this.state = {
      citySelected: '',
    };

    // this.handleCountryChange = this.handleCountryChange.bind(this);
    // this.handleStateChange = this.handleStateChange.bind(this);
    // this.handleCityChange = this.handleCityChange.bind(this);
  }

  // componentWillMount() {
  //   this.props.getCountries();
  // }

  handleCountryChange(evt) {
    // this.props.getStates(evt.value);
  }

  handleStateChange(e) {
    // this.props.getCities(e.value);
  }

  handleCityChange(e) {
    // this.props.selectCity(e.value, this._address.value);
  }

  render() {
    // const getaddress = undefined;
    // const profile = undefined;
    // const { getaddress, profile } = this.props;
    // if (getaddress.country !== undefined) {
    //   countries = [];
    //   getaddress.country.map((country) =>
    //     countries.push({
    //       value: country.cid,
    //       label: country.name,
    //     })
    //   );
    // }
    // if (getaddress.state !== undefined) {
    //   states = [];
    //   getaddress.state.map((item) =>
    //     states.push({
    //       value: item.sid,
    //       label: item.name,
    //     })
    //   );
    // }
    // if (getaddress.city !== undefined) {
    //   cities = [];
    //   getaddress.city.map((item) =>
    //     cities.push({
    //       value: item.ctid,
    //       label: item.name,
    //     })
    //   );
    // }
    return (
      <div>
        <div className="collapse" id="addressCollapse">
          <div className="form-group row">
            <label className="col-sm-3 offset-sm-1 col-form-label">
              <div className="pull-left">Quốc Gia</div>
            </label>
            <Select
              className="col-sm-6 react-select-container"
              options={countries}
              onChange={this.handleCountryChange}
            />
          </div>
          <div className="form-group row">
            <label className="col-sm-3 offset-sm-1 col-form-label">
              <div className="pull-left">Tỉnh/Thành Phố</div>
            </label>
            <Select
              className="col-sm-6 react-select-container"
              options={states}
              onChange={this.handleStateChange}
            />
          </div>

          <div className="form-group row">
            <label className="col-sm-3 offset-sm-1 col-form-label">
              <div className="pull-left">Quận/Huyện</div>
            </label>
            <Select
              className="col-sm-6 react-select-container"
              options={cities}
              onChange={this.handleCityChange}
            />
          </div>
          <div className="form-group row">
            <label className="col-sm-3 offset-sm-1 col-form-label">
              <div className="pull-left">Địa Chỉ Chi Tiết *</div>
            </label>
            <input
              type="text"
              className="form-control col-sm-6"
              // defaultValue={profile.profile['address']}
              ref={(input) => (this._address = input)}
            />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { getaddress, profile } = state;
  return { getaddress, profile };
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      // getCountries,
      // getStates,
      // getCities,
      // selectCity,
    },
    dispatch
  );

const connectedCountry = connect(
  mapStateToProps,
  mapDispatchToProps
)(Address);
export { connectedCountry as Address };
