export default {
  apiUrl: 'http://yoursite.com/api/',
};

const siteConfig = {
  siteName: 'StyleSeat',
  siteIcon: 'ion-flash',
  footerText: '©2019 KIS-V',
};
const themeConfig = {
  topbar: 'themedefault',
  sidebar: 'themedefault',
  layout: 'themedefault',
  theme: 'themedefault',
};
const language = 'english';

const jwtConfig = {
  fetchUrl: '/api/',
  secretKey: 'secretKey',
};
const googleConfig = {
  apiKey: 'AIzaSyBnDzr7nNaXBFYMTyH5fBuwsC1GsGmv6GE', //
};

export { siteConfig, language, themeConfig, jwtConfig, googleConfig };
