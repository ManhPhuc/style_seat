import { getToken } from '../helpers/utility';
let HEADERS = {};

const initHeader = () => {
  const TOKEN = 'Bearer ' + getToken().get('idToken');
  return (HEADERS = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: TOKEN.replace(new RegExp('"', 'g'), ''),
  });
};

const Fetch = (url, method = 'GET', header = HEADERS, data = {}) => {
  return fetch(url, {
    method: method,
    headers: header,
    body: JSON.stringify(data),
  });
};

const Get = (url, data = {}, header = initHeader()) => {
  return fetch(url, {
    method: 'GET',
    headers: header,
  });
};

const PostImage = (url, data = {}) => {
  const TOKEN = 'Bearer ' + getToken().get('idToken');
  HEADERS = {
    Accept: '*/*',
    Authorization: TOKEN.replace(new RegExp('"', 'g'), ''),
  };
  return fetch(url, {
    method: 'POST',
    headers: HEADERS,
    body: data,
  });
};

const Post = (url, data = {}, header = initHeader()) => {
  return Fetch(url, 'POST', header, data);
};

const Put = (url, data = {}, header = initHeader()) => {
  return Fetch(url, 'PUT', header, data);
};

const Delete = (url, data = {}, header = initHeader()) => {
  return Fetch(url, 'DELETE', header, data);
};

export const HTTP = {
  Get,
  Put,
  Post,
  Delete,
  Fetch,
  PostImage,
};
