import React, { Component } from 'react';
import { AddUser } from '../../components/user-profile-form/add-user.js';
import LayoutContentWrapper from '../../components/utility/layoutWrapper';
import LayoutContent from '../../components/utility/layoutContent';

export default class WorkerProfile extends Component {
  render() {
    return (
      <LayoutContentWrapper>
        <LayoutContent>
          <AddUser />
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}
