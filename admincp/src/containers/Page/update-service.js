import React, { Component } from 'react';
import { UpdateService } from '../../components/service-form/update-service.js';
import LayoutContentWrapper from '../../components/utility/layoutWrapper';
import LayoutContent from '../../components/utility/layoutContent';

export default class WorkerProfile extends Component {
  render() {
    return (
      <LayoutContentWrapper>
        <LayoutContent>
          <UpdateService />
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}
