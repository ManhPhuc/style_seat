import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Form, Icon, Input, Checkbox, Button } from 'antd';
import authAction from '../../redux/auth/actions';
import IntlMessages from '../../components/utility/intlMessages';
import SignInStyleWrapper from './signin.style';

const { login } = authAction;

class SignIn extends Component {
  state = {
    redirectToReferrer: false,
  };

  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { login } = this.props;
        const username = this._username.state.value;
        const password = this._password.state.value;
        login(username, password);
      }
    });
  };

  render() {
    const { Auth } = this.props;
    const fromShow = { pathname: '/dashboard' };
    const { redirectToReferrer } = this.state;
    const { getFieldDecorator } = this.props.form;

    if (redirectToReferrer) {
      return <Redirect to={fromShow} />;
    }
    return (
      <SignInStyleWrapper className="isoSignInPage">
        <div className="isoLoginContentWrapper">
          <div className="isoLoginContent">
            <div className="isoLogoWrapper">
              <Link to="/dashboard">
                <IntlMessages id="page.signInTitle" />
              </Link>
            </div>

            <div className="isoSignInForm">
              <div>
                <Form onSubmit={this.handleSubmit} className="login-form">
                  {Auth !== undefined ? (
                    <div
                      className="form-group"
                      style={{ display: 'flex', justifyContent: 'center' }}
                    >
                      <span style={{ color: '#ff0000' }}>{Auth.error}</span>
                    </div>
                  ) : (
                    true
                  )}

                  <Form.Item>
                    {getFieldDecorator('Email', {
                      rules: [
                        {
                          type: 'email',
                          message: 'The input is not valid E-mail!',
                        },
                        {
                          required: true,
                          message: 'Please input your E-mail!',
                        },
                      ],
                    })(
                      <Input
                        prefix={
                          <Icon
                            type="mail"
                            style={{ color: 'rgba(0,0,0,.25)' }}
                          />
                        }
                        placeholder="Email"
                        ref={(Input) => (this._username = Input)}
                      />
                    )}
                  </Form.Item>
                  <Form.Item>
                    {getFieldDecorator('password', {
                      rules: [
                        {
                          required: true,
                          message: 'Please input your Password!',
                        },
                      ],
                    })(
                      <Input
                        prefix={
                          <Icon
                            type="lock"
                            style={{ color: 'rgba(0,0,0,.25)' }}
                          />
                        }
                        type="password"
                        placeholder="Password"
                        ref={(Input) => (this._password = Input)}
                      />
                    )}
                  </Form.Item>
                  <Form.Item>
                    <div className="isoInputWrapper isoLeftRightComponent">
                      {getFieldDecorator('remember', {
                        valuePropName: 'checked',
                        initialValue: false,
                      })(
                        <Checkbox>
                          <IntlMessages id="page.signInRememberMe" />
                        </Checkbox>
                      )}
                      <Button
                        type="primary"
                        htmlType="submit"
                        className="login-form-button"
                      >
                        Log in
                      </Button>
                    </div>
                  </Form.Item>
                </Form>
              </div>

              <div className="isoInputWrapper isoOtherLogin">
                <Button onClick={this.handleLogin} type="primary btnFacebook">
                  <IntlMessages id="page.signInFacebook" />
                </Button>
                <Button onClick={this.handleLogin} type="primary btnGooglePlus">
                  <IntlMessages id="page.signInGooglePlus" />
                </Button>
              </div>
              <div className="isoCenterComponent isoHelperWrapper">
                <Link to="" className="isoForgotPass">
                  <IntlMessages id="page.signInForgotPass" />
                </Link>
                <Link to="">
                  <IntlMessages id="page.signInCreateAccount" />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </SignInStyleWrapper>
    );
  }
}

const WrappedSignIn = Form.create({ name: 'normal_login' })(SignIn);

function mapStateToProps(state) {
  const { Auth } = state;
  return { Auth };
}

export default connect(
  // (state) => ({
  //   isLoggedIn: state.auth.idToken !== null ? true : false,
  mapStateToProps,
  // }),

  { login }
)(WrappedSignIn);
