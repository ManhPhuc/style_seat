import React, { Component } from 'react';
import { AddService } from '../../components/service-form/add-service.js';
import LayoutContentWrapper from '../../components/utility/layoutWrapper';
import LayoutContent from '../../components/utility/layoutContent';

export default class WorkerProfile extends Component {
  render() {
    return (
      <LayoutContentWrapper>
        <LayoutContent>
          <AddService />
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}
