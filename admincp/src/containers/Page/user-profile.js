import React, { Component } from 'react';
import { UserProfile } from '../../components/user-profile-form/user-profile.js';
import LayoutContentWrapper from '../../components/utility/layoutWrapper';
import LayoutContent from '../../components/utility/layoutContent';

export default class WorkerProfile extends Component {
  render() {
    return (
      <LayoutContentWrapper>
        <LayoutContent>
          <UserProfile />
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}
