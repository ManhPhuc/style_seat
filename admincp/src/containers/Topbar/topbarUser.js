import React, { Component } from 'react';
import { connect } from 'react-redux';
import Popover from '../../components/uielements/popover';
import IntlMessages from '../../components/utility/intlMessages';
import authAction from '../../redux/auth/actions';
import TopbarDropdownWrapper from './topbarDropdown.style';
import { Avatar } from 'antd';
import { getUid } from '../../helpers/utility';

const { logout, getAvatarAdmin } = authAction;
class TopbarUser extends Component {
  constructor(props) {
    super(props);
    this.handleVisibleChange = this.handleVisibleChange.bind(this);
    this.hide = this.hide.bind(this);
    this.state = {
      visible: false,
    };
  }
  hide() {
    this.setState({ visible: false });
  }
  handleVisibleChange() {
    this.setState({ visible: !this.state.visible });
  }

  componentWillMount() {
    const uida = getUid().get('uid');
    this.props.getAvatarAdmin(uida);
  }

  render() {
    let avatar = null;
    const avatar2 = this.props.Auth.avatar;
    if (avatar2 !== undefined && avatar2[0] !== undefined) {
      avatar = avatar2[0]['url'];
    }
    const content = (
      <TopbarDropdownWrapper className="isoUserDropdown">
        <a className="isoDropdownLink" href="# ">
          <IntlMessages id="themeSwitcher.settings" />
        </a>
        <a className="isoDropdownLink" href="# ">
          <IntlMessages id="sidebar.feedback" />
        </a>
        <a className="isoDropdownLink" href="# ">
          <IntlMessages id="topbar.help" />
        </a>
        <a className="isoDropdownLink" onClick={this.props.logout} href="# ">
          <IntlMessages id="topbar.logout" />
        </a>
      </TopbarDropdownWrapper>
    );

    return (
      <Popover
        content={content}
        trigger="click"
        visible={this.state.visible}
        onVisibleChange={this.handleVisibleChange}
        arrowPointAtCenter={true}
        placement="bottomLeft"
      >
        <div className="isoImgWrapper">
          <Avatar
            size={40}
            src={avatar !== undefined ? avatar : 'https://images.pexels.com/photos/459947/pexels-photo-459947.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'}
          />
          <span className="userActivity online" />
        </div>
      </Popover>
    );
  }
}

function mapStateToProps(state) {
  const { Auth } = state;
  return { Auth };
}

export default connect(
  mapStateToProps,
  { logout, getAvatarAdmin }
)(TopbarUser);
