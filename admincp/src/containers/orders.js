import React from 'react';
import { OrderTable } from '../components/order-table';

export default class Demo extends React.Component {
  render() {
    return (
      <div>
        <OrderTable />
      </div>
    );
  }
}
