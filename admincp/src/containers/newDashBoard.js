import React, { Component } from 'react';
import LayoutWrapper from '../components/utility/layoutWrapper.js';
import StickerWidget from './Widget/sticker/sticker-widget';
import IsoWidgetsWrapper from './Widget/widgets-wrapper';
import { Row, Col } from 'antd';
import GoogleMap from './GoogleMap/googleMap';
import ReCharts from './Chart/index.js';

export default class Dashboard extends Component {
    render() {
        const { icon, iconcolor, number, text, rowStyle, colStyle } = this.props;
        const iconStyle = {
            color: iconcolor,
        };
        const wisgetPageStyle = {
            display: 'flex',
            flexFlow: 'row wrap',
            alignItems: 'flex-start',
            overflow: 'hidden',
        };
        const outlookStyle = {
            width: '100%',
            margin: '0 auto'
        };
        return (
            <LayoutWrapper>
                <h1>Dashboard Style Seat</h1>

                <Row style={outlookStyle} gutter={0} justify="start">
                    <Col offset={3} lg={6} md={4} sm={4} xs={4} style={colStyle}>
                        <IsoWidgetsWrapper>
                            {/* Sticker Widget */}
                            <StickerWidget
                                number={"210"}
                                text={"New Orders"}
                                icon="ion-android-cart"
                                fontColor="#000"
                                bgColor="#e5e085"
                            />
                        </IsoWidgetsWrapper>
                    </Col>

                    <Col lg={6} md={4} sm={4} xs={4} style={colStyle}>
                        <IsoWidgetsWrapper>
                            {/* Sticker Widget */}
                            <StickerWidget
                                number={"25"}
                                text={"New Clients"}
                                icon="ion-ios-people"
                                fontColor="#000"
                                bgColor="#42A5F6"
                            />
                        </IsoWidgetsWrapper>
                    </Col>

                    <Col lg={6} md={4} sm={4} xs={4} style={colStyle}>
                        <IsoWidgetsWrapper>
                            {/* Sticker Widget */}
                            <StickerWidget
                                number={"56"}
                                text={"New Workers"}
                                icon="ion-android-person-add"
                                fontColor="#000"
                                bgColor="#e5a032"
                            />
                        </IsoWidgetsWrapper>
                    </Col>
                </Row>

                {/* MAP */}
                <Row style={outlookStyle}>
                    < GoogleMap />
                </Row>

                {/* CHARS */}
                <Row style={outlookStyle}>
                    <ReCharts />
                </Row>
            </LayoutWrapper>
        );
    }
}