import React, { Component } from 'react';
import Table, {
  TableBody,
  TableRow,
  TableCell,
  TableHead, TablePagination, TableSortLabel
} from 'material-ui/Table';
import classNames from "classnames";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import DeleteIcon from "@material-ui/icons/Delete";
import FilterListIcon from "@material-ui/icons/FilterList";
import { lighten } from "@material-ui/core/styles/colorManipulator";

let counter = 0;
function createData(firstname, lastname, birthday, gender, address, email,phone, type, status) {
  counter += 1;
  return { id: counter,  firstname, lastname, birthday, gender, address, email,phone, type, status};
}



function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === "desc"
    ? (a, b) => desc(a, b, orderBy)
    : (a, b) => -desc(a, b, orderBy);
}

const rows = [
  {
    id: "id",
    numeric: false,
    disablePadding: true,
    label: "ID"
  },
  { id: "firstname", numeric: true, disablePadding: false, label: "First Name" },
  { id: "lastname", numeric: true, disablePadding: false, label: "Last Name" },
  { id: "birthday", numeric: true, disablePadding: false, label: "Birth Day" },
  { id: "gender", numeric: true, disablePadding: false, label: "Gender" },
  { id: "email", numeric: true, disablePadding: false, label: "Email" },
  { id: "phone", numeric: true, disablePadding: false, label: "Phone" },
  { id: "type", numeric: true, disablePadding: false, label: "Type" },
  { id: "status", numeric: true, disablePadding: false, label: "Status" }
];

class EnhancedTableHead extends Component {
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const {
      onSelectAllClick,
      order,
      orderBy,
      numSelected,
      rowCount
    } = this.props;

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell>
          {rows.map(
            row => (
              <TableCell
                key={row.id}
                align={row.numeric ? "right" : "left"}
                padding={row.disablePadding ? "none" : "default"}
                sortDirection={orderBy === row.id ? order : false}
              >
                <Tooltip
                  title="Sort"
                  placement={row.numeric ? "bottom-end" : "bottom-start"}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === row.id}
                    direction={order}
                    onClick={this.createSortHandler(row.id)}
                  >
                    {row.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            ),
            this
          )}
        </TableRow>
      </TableHead>
    );
  }
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number,
  onRequestSort: PropTypes.func,
  onSelectAllClick: PropTypes.func,
  order: PropTypes.string,
  orderBy: PropTypes.string,
  rowCount: PropTypes.number
};

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit
  },
  highlight:
    theme.palette.type === "light"
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85)
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark
        },
  spacer: {
    flex: "1 1 100%"
  },
  actions: {
    color: theme.palette.text.secondary
  },
  title: {
    flex: "0 0 auto"
  }
});

let EnhancedTableToolbar = props => {
  const { numSelected, classes } = props;

  return (
    <Toolbar
      className={classNames(classes.root, {
        [classes.highlight]: numSelected > 0
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography color="inherit" variant="subtitle1">
            {numSelected} selected
          </Typography>
        ) : (
          <Typography variant="h6" id="tableTitle">
            USER MANAGE
          </Typography>
        )}
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        {numSelected > 0 ? (
          <Tooltip title="Delete">
            <IconButton aria-label="Delete">
              <DeleteIcon />
            </IconButton>
          </Tooltip>
          
        ) : (
          <Tooltip title="Filter list">
            <IconButton aria-label="Filter list">
              <FilterListIcon />
            </IconButton>
          </Tooltip>
        )}
      </div>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3
  },
  table: {
    minWidth: 1020
  },
  tableWrapper: {
    overflowX: "auto"
  }
});

class EnhancedTable extends Component {
  constructor(props) {
    super(props);

    //  this.state.products = [];
    this.state = {};
    this.state.filterText = "";
    this.state = {
    order: "asc",
    orderBy: "calories",
    selected: [],
    data: [
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active"),
      createData("Trinh","Giang","25/10/1995", "Female", "Bach Mai", "giang@gmail.com", "31285", "Worker","Active")
    ],
    page: 0,
    rowsPerPage: 5
  };
  }

  handleUserInput(filterText) {
    this.setState({filterText: filterText});
  };
  handleRowDel(data) {
    var index = this.state.data.indexOf(data);
    this.state.data.splice(index, 1);
    this.setState(this.state.data);
  };



  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = "desc";

    if (this.state.orderBy === property && this.state.order === "desc") {
      order = "asc";
    }

    this.setState({ order, orderBy });
  };

  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.setState(state => ({ selected: state.data.map(n => n.id) }));
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { classes } = this.props;
    const { data, order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows =
      rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);
    console.log(page);

    return (
      <Paper className={classes.root}>
        <EnhancedTableToolbar numSelected={selected.length} />
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
            />
            <TableBody>
              {stableSort(data, getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(n => {
                  const isSelected = this.isSelected(n.id);
                  return (
                    <TableRow
                      hover
                      onClick={event => this.handleClick(event, n.id)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={n.id}
                      selected={isSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox checked={isSelected} />
                      </TableCell>
                      <TableCell align="right">{n.id}</TableCell>
                      <TableCell align="right">{n.firstname}</TableCell>
                      <TableCell align="right">{n.lastname}</TableCell>
                      <TableCell align="right">{n.birthday}</TableCell>
                      <TableCell align="right">{n.address}</TableCell>
                      <TableCell align="right">{n.email}</TableCell>
                      <TableCell align="right">{n.phone}</TableCell>
                      <TableCell align="right">{n.type}</TableCell>
                      <TableCell align="right">{n.status}</TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            "aria-label": "Previous Page"
          }}
          nextIconButtonProps={{
            "aria-label": "Next Page"
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    );
  }
}

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(EnhancedTable);

