const options = [
  {
    key: 'adminProfile',
    label: 'sidebar.adminProfile',
    leftIcon: 'ion-user',
  },
  {
    key: 'user',
    label: 'sidebar.user',
    leftIcon: 'ion',
  },
  {
    key: 'order',
    label: 'sidebar.order',
    leftIcon: 'ion',
  },
  {
    key: 'services',
    label: 'sidebar.services',
    leftIcon: 'ion',
  },
];
export default options;
