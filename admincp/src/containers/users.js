import React from 'react';
import { UserTable } from '../components/user-profile-form';

export default class Demo extends React.Component {
  render() {
    return (
      <div>
        <UserTable />
      </div>
    );
  }
}


// import React, { Component } from 'react';
// import Table, {
//   TableBody,
//   TableRow,
//   TableCell,
//   TableHead,
//   TablePagination,
//   TableSortLabel,
// } from 'material-ui/Table';
// import classNames from 'classnames';
// import PropTypes from 'prop-types';
// import { withStyles } from '@material-ui/core/styles';
// import Toolbar from '@material-ui/core/Toolbar';
// import Typography from '@material-ui/core/Typography';
// import Paper from '@material-ui/core/Paper';
// import Checkbox from '@material-ui/core/Checkbox';
// import IconButton from '@material-ui/core/IconButton';
// import Tooltip from '@material-ui/core/Tooltip';
// import DeleteIcon from '@material-ui/icons/Delete';
// import EditIcon from '@material-ui/icons/Edit'
// import FilterListIcon from '@material-ui/icons/FilterList';
// import { lighten } from '@material-ui/core/styles/colorManipulator';
// import SearchIcon from '@material-ui/icons/Search';
// import InputBase from '@material-ui/core/InputBase';
// import { Button } from '@material-ui/core';
// import { Link } from 'react-router-dom';
// import { getUsers } from '../redux/user/action';
// import { bindActionCreators } from 'redux';
// import { connect } from 'react-redux';
// import { Empty, Spin, Input, Icon } from 'antd';
// // import Icon from '@material-ui/core/Icon';

// function desc(a, b, orderBy) {
//   if (b[orderBy] < a[orderBy]) {
//     return -1;
//   }
//   if (b[orderBy] > a[orderBy]) {
//     return 1;
//   }
//   return 0;
// }

// function stableSort(array, cmp) {
//   const stabilizedThis = array.map((el, index) => [el, index]);
//   stabilizedThis.sort((a, b) => {
//     const order = cmp(a[0], b[0]);
//     if (order !== 0) return order;
//     return a[1] - b[1];
//   });
//   return stabilizedThis.map((el) => el[0]);
// }

// function getSorting(order, orderBy) {
//   return order === 'desc'
//     ? (a, b) => desc(a, b, orderBy)
//     : (a, b) => -desc(a, b, orderBy);
// }

// const rows = [
//   { id: 'uid', numeric: false, disablePadding: false, label: <b>ID</b> },
//   { id: 'email', numeric: true, disablePadding: false, label: <b>Email</b> },
//   { id: 'phone', numeric: true, disablePadding: false, label: <b>SĐT</b>},
//   { id: 'firstname', numeric: true, disablePadding: false, label: <b>Họ</b> },
//   { id: 'lastname', numeric: true, disablePadding: false, label: <b>Tên</b> },
//   { id: 'birthday', numeric: true, disablePadding: false, label: <b>Ngày Sinh</b>},
//   { id: 'address', numeric: true, disablePadding: false, label: <b>Địa Chỉ</b>},
//   { id: 'status', numeric: true, disablePadding: false, label: <b>Thao Tác</b>},
// ];

// class EnhancedTableHead extends Component {
//   constructor(props) {
//     super(props);
//   }
//   createSortHandler = (property) => (event) => {
//     this.props.onRequestSort(event, property);
//   };

//   render() {
//     const {
//       onSelectAllClick,
//       order,
//       orderBy,
//       numSelected,
//       rowCount,
//     } = this.props;

//     return (
//       <TableHead>
//         <TableRow>
//           <TableCell padding="checkbox">
//             <Checkbox
//               indeterminate={numSelected > 0 && numSelected < rowCount}
//               checked={numSelected === rowCount}
//               onChange={onSelectAllClick}
//             />
//           </TableCell>
//           {rows.map(
//             (row) => (
//               <TableCell
//                 key={row.id}
//                 align={row.numeric ? 'right' : 'left'}
//                 padding={row.disablePadding ? 'none' : 'default'}
//                 sortDirection={orderBy === row.id ? order : false}
//               >
//                 <Tooltip
//                   title="Sắp xếp"
//                   placement={row.numeric ? 'bottom-end' : 'bottom-start'}
//                   enterDelay={300}
//                 >
//                   <TableSortLabel
//                     active={orderBy === row.id}
//                     direction={order}
//                     onClick={this.createSortHandler(row.id)}
//                   >
//                     {row.label}
//                   </TableSortLabel>
//                 </Tooltip>
//               </TableCell>
//             ),
//             this
//           )}
//         </TableRow>
//       </TableHead>
//     );
//   }
// }

// EnhancedTableHead.propTypes = {
//   numSelected: PropTypes.number,
//   onRequestSort: PropTypes.func,
//   onSelectAllClick: PropTypes.func,
//   order: PropTypes.string,
//   orderBy: PropTypes.string,
//   rowCount: PropTypes.number,
// };

// const toolbarStyles = (theme) => ({
//   root: {
//     paddingRight: theme.spacing.unit,
//   },
//   highlight:
//     theme.palette.type === 'light'
//       ? {
//         color: theme.palette.secondary.main,
//         backgroundColor: lighten(theme.palette.secondary.light, 0.85),
//       }
//       : {
//         color: theme.palette.text.primary,
//         backgroundColor: theme.palette.secondary.dark,
//       },
//   actions: {
//     color: theme.palette.text.secondary,
//   },
//   title: {
//     flex: '0 0 auto',
//   },
//   absolute: {
//     position: 'absolute',
//     bottom: theme.spacing.unit * 1,
//     right: theme.spacing.unit * 3,
//   },
// });

// let EnhancedTableToolbar = (props) => {
//   const { numSelected , url, classes } = props;
//   const key = 'add-user'

//   return (
//     <Toolbar
//       className={classNames(classes.root, {
//         [classes.highlight]: numSelected > 0,
//       })}
//     >
//       <div className={classes.title}>
//         {numSelected > 0 ? (
//           <Typography color="inherit" variant="subtitle1">
//             {numSelected} selected
//           </Typography>
//         ) : (
//             <Typography variant="h6" id="tableTitle">
//               Quản Lý Tài Khoản
//           </Typography>
//           )}
//       </div>
//       <div className={classes.actions}>
//         {numSelected > 0 ? (
//           <div className={classes.absolute}>
//             <Tooltip title="Delete">
//               <IconButton aria-label="Delete">
//                 <DeleteIcon />
//               </IconButton>
//             </Tooltip>
//           </div>
//         ) : (
//             <div>
//               <Tooltip title="Thêm" aria-label="Thêm">
//                 <Button
//                   className={classes.absolute}
//                   aria-label="Thêm"
//                 >
//                   <Link to={`${url}/${key}`}><i className="fas fa-2x fa-plus-circle"></i></Link>
//                 </Button>
//               </Tooltip>
//             </div>
//           )}
//       </div>
//     </Toolbar>
//   );
// };

// EnhancedTableToolbar.propTypes = {
//   classes: PropTypes.object.isRequired,
//   numSelected: PropTypes.number.isRequired,
// };

// EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

// const styles = (theme) => ({
//   root: {
//     width: '100%',
//     marginTop: theme.spacing.unit * 3,
//   },
//   table: {
//     minWidth: 1020,
//   },
//   tableWrapper: {
//     overflowX: 'auto',
//   },
// });

// class EnhancedTable extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {};
//     this.state.filterText = '';
//     this.state = {
//       order: 'asc',
//       orderBy: 'calories',
//       selected: [],
//       data: [],
//       page: 0,
//       rowsPerPage: 10,
//     };
//   }

//   handleUserInput(filterText) {
//     this.setState({ filterText: filterText });
//   }
//   handleRowDel(data) {
//     var index = this.state.data.indexOf(data);
//     this.state.data.splice(index, 1);
//     this.setState(this.state.data);
//   }
//   handleRequestSort = (event, property) => {
//     const orderBy = property;
//     let order = 'desc';

//     if (this.state.orderBy === property && this.state.order === 'desc') {
//       order = 'asc';
//     }

//     this.setState({ order, orderBy });
//   };

//   handleSelectAllClick = (event) => {
//     if (event.target.checked) {
//       this.setState((state) => ({ selected: this.props.users.users.map((n) => n.uid) }));
//       return;
//     }
//     this.setState({ selected: [] });
//   };

//   handleClick = (event, id) => {
//     const { selected } = this.state;
//     const selectedIndex = selected.indexOf(id);
//     let newSelected = [];

//     if (selectedIndex === -1) {
//       newSelected = newSelected.concat(selected, id);
//     } else if (selectedIndex === 0) {
//       newSelected = newSelected.concat(selected.slice(1));
//     } else if (selectedIndex === selected.length - 1) {
//       newSelected = newSelected.concat(selected.slice(0, -1));
//     } else if (selectedIndex > 0) {
//       newSelected = newSelected.concat(
//         selected.slice(0, selectedIndex),
//         selected.slice(selectedIndex + 1)
//       );
//     }

//     this.setState({ selected: newSelected });
//   };

//   componentWillMount() {
//     this.props.getUsers();
//   }

//   handleChangePage = (event, page) => {
//     this.setState({ page });
//   };

//   handleChangeRowsPerPage = (event) => {
//     this.setState({ rowsPerPage: event.target.value });
//   };

//   isSelected = (id) => this.state.selected.indexOf(id) !== -1;

//   render() {
//     const stripTrailingSlash = (str) => {
//       if (str.substr(-1) === '/') {
//         return str.substr(0, str.length - 1);
//       }
//       return str;
//     };
//     const user = this.props.users.users;
//     if (!user) {
//       return (
//         <div>
//           <Spin>
//             <Empty />
//           </Spin>
//         </div>
//       );
//     } else if (!user[0]) {
//       return (
//         <div>
//           <h5>Chưa có dữ liệu</h5>
//         </div>
//       )
//     }
//     const { classes } = this.props;
//     const data = this.props.users.users;
//     const url = stripTrailingSlash(this.props.location.pathname);
//     const { order, orderBy, selected, rowsPerPage, page } = this.state;
//     const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);
//     const key = 'user-profile';
//     return (
//       <Paper className={classes.root}>
//         <EnhancedTableToolbar numSelected={selected.length} url={url} />
//         <div className={classes.tableWrapper}>
//           <div>
//             <Input
//               addonAfter={<Icon type="search" />}
//               placeholder="Seach"
//               style={{ marginTop: '2%' }}
//              />
//           </div>
//           <Table className={classes.table} aria-labelledby="tableTitle">
//             <EnhancedTableHead
//               numSelected={selected.length}
//               order={order}
//               orderBy={orderBy}
//               onSelectAllClick={this.handleSelectAllClick}
//               onRequestSort={this.handleRequestSort}
//               rowCount={data.length}
//             />
//             <TableBody>
//               {stableSort(data, getSorting(order, orderBy))
//                 .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
//                 .map((n) => {
//                   const isSelected = this.isSelected(n.uid);
//                   return (
//                     <TableRow
//                       hover
//                       role="checkbox"
//                       aria-checked={isSelected}
//                       tabIndex={-1}
//                       key={n.uid}
//                       selected={isSelected}
//                     >
//                       <TableCell
//                         onClick={(event) => this.handleClick(event, n.uid)}
//                         padding="checkbox"
//                       >
//                         <Checkbox checked={isSelected} />
//                       </TableCell>
//                       <TableCell align="right">{n.uid}</TableCell>
//                       <TableCell align="right">{n.email}</TableCell>
//                       <TableCell align="right">{n.phone}</TableCell>
//                       <TableCell align="right">{n.firstName}</TableCell>
//                       <TableCell align="right">{n.lastName}</TableCell>
//                       <TableCell align="right">{n.birthDay}</TableCell>
//                       <TableCell align="right">{n.address}</TableCell>
//                       <TableCell>
//                         <Link to={`${url}/${key}/${n.uid}`}>
//                           <Tooltip title="Sửa">
//                             <IconButton aria-label="Sửa">
//                               <EditIcon />
//                             </IconButton>
//                           </Tooltip>
//                         </Link>
//                       </TableCell>
//                     </TableRow>
//                   );
//                 })}
//               {emptyRows > 0 && (
//                 <TableRow style={{ height: 49 * emptyRows }}>
//                   <TableCell colSpan={6} />
//                 </TableRow>
//               )}
//             </TableBody>
//           </Table>
//         </div>
//         <TablePagination
//           rowsPerPageOptions={[5, 10, 25]}
//           component="div"
//           count={data.length}
//           rowsPerPage={rowsPerPage}
//           page={page}
//           backIconButtonProps={{
//             'aria-label': 'Previous Page',
//           }}
//           nextIconButtonProps={{
//             'aria-label': 'Next Page',
//           }}
//           onChangePage={this.handleChangePage}
//           onChangeRowsPerPage={this.handleChangeRowsPerPage}
//         />
//       </Paper>
//     );
//   }
// }

// EnhancedTable.propTypes = {
//   classes: PropTypes.object.isRequired,
// };

// function mapStateToProps(state) {
//   const { users } = state;
//   return { users };
// }

// const mapDispatchToProps = (dispatch) =>
//   bindActionCreators({
//     getUsers,
//   },
//     dispatch
//   );

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(withStyles(styles)(EnhancedTable));