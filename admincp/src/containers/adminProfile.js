import React, { Component } from 'react';
import {
  Row,
  Col,
  Form,
  Empty,
  Spin,
  Input,
  Button,
  Modal,
  Select,
  Avatar,
  Tooltip,
} from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  getProfile,
  getAvatar,
  updateProfile,
  receiveUpdateProfile,
} from '../redux/admin/action';
import { Address } from '../components/address';
import { Password } from '../components/password';
import { getUid } from '../helpers/utility';
import * as API from '../services/api';
import { history } from '../redux/store';

const env = process.env.REACT_APP_ENV || process.env.NODE_ENV || 'development';
const config = require('../config/config.json')[env];
const Option = Select.Option;
const status = [
  { key: 1, value: 'active', label: 'Kích hoạt', color: '#00BB00' },
  { key: 2, value: 'inactive', label: 'Khóa', color: '#FF0000' },
  { key: 3, value: 'pending', label: 'Chờ xử lý', color: '#FFAA00' },
  { key: 4, value: 'ban', label: 'Cấm', color: '#FF0000' },
];
const type = [
  { key: 1, value: 'ROLE_GENERALMANAGER', label: 'Quản trị', color: '#BC0900' },
  { key: 2, value: 'ROLE_MANAGER', label: 'Quản lý', color: '#DF1000' },
  { key: 3, value: 'ROLE_WORKER', label: 'Thợ', color: '#FF4A00' },
  { key: 4, value: 'ROLE_CLIENT', label: 'Khách', color: '#FF7F00' },
];
const gender = [{ key: 0, label: 'Nam' }, { key: 1, label: 'Nữ' }];
let listAva = null;
let data = null;
const uidAdmin = getUid().get('uid');
const url = '/dashboard';
class AdminProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gender: '',
      type: '',
      status: '',
      modalAvatar: false,
      modalProfile: false,
      message: '',
    };
  }

  componentWillMount() {
    this.props.getProfile(uidAdmin);
    this.props.getAvatar(uidAdmin);
  }

  // handleSubmit = (e) => {
  //   e.preventDefault();
  //   this.props.form.validateFields((err, values) => {
  //     if (!err){
  //       this.updateUser;
  //     }
  //   });
  // }

  getGender = (e) => {
    this.setState({ gender: e });
  };

  getType = (e) => {
    this.setState({ type: e });
  };

  getStatus = (e) => {
    this.setState({ status: e });
  };

  updateUser = () => {
    const firstName = this._firstName.state.value;
    const lastName = this._lastName.state.value;
    const birthDay = this._birthDay.state.value;
    const genderF = this._gender.props.defaultValue;
    const genderS = this.state.gender;
    const ctid = 1;
    const address = 'Hà Nội';

    const email = this._email.state.value;
    const phone = this._phone.state.value;
    const typeF = this._type.props.defaultValue;
    const typeS = this.state.type;
    const statusF = this._status.props.defaultValue;
    const statusS = this.state.status;

    const gender = genderS !== '' ? genderS : genderF;
    const type = typeS !== '' ? typeS : typeF;
    const status = statusS !== '' ? statusS : statusF;
    const uid = uidAdmin;

    const profile = {uid, firstName, lastName, birthDay, gender, ctid, address };
    const user = {uid, email, phone, type, status };
    const data = { profile, user };

    this.props.updateProfile(data);
    this.setState({ modalProfile: true });
  };

  onHandleFileChange = (e) => {
    let formData = new FormData();
    if (e.target.id === 'uploadAva') {
      formData.append('avatar', e.target.files[0]);
    }
    const link = config.base_api + '/images';
    API.postImage(formData, link).then((res) => {
      this.setState({ modalAvatar: true, message: res['message'] });
      this.props.getAvatar(uidAdmin);
    });
  };

  addAvatar = (e) => {
    e.preventDefault();
    const ele = document.getElementById('uploadAva');
    ele.click(e);
  };

  setModalAvatar(modalAvatar) {
    this.setState({ modalAvatar });
  }

  setModalProfile(modalProfile) {
    this.setState({ modalProfile });
  }

  render() {
    // const { getFieldDecorator } = this.props.form;
    const { profile, avatar } = this.props.admin;
    if (avatar !== undefined) {
      listAva = avatar;
    }
    if (profile !== undefined) {
      data = profile;
    }

    if (listAva == null || data == null) {
      return (
        <div>
          <Spin>
            <Empty />
          </Spin>
        </div>
      );
    }

    return (
      <div>
        <Modal
          title="Thông Báo"
          visible={this.state.modalAvatar}
          onOk={() => this.setModalAvatar(false)}
          onCancel={() => this.setModalAvatar(false)}
        >
          {this.state.message == 'Save images sucessfully' ? <p>Cập nhật Avatar thành công!</p> : <p>Cập nhật thất bại!</p>}
        </Modal>

        <Modal
          title="Thông Báo"
          visible={this.state.modalProfile}
          onOk={() => this.setModalProfile(false)}
          onCancel={() => this.setModalProfile(false)}
        >
          {this.props.admin.message == 'Update user successfully' ? (
            <p>Cập nhật tài khoản thành công!</p>
          ) : (
              <p>Cập nhật tài khoản thất bại!</p>
            )}
        </Modal>
        <h6>Thông tin tài khoản</h6>
        <div style={{ textAlign: 'center' }}>
          <Tooltip title="Chọn để sửa">
            <Avatar
              onClick={this.addAvatar}
              size={77}
              src={
                listAva[0] !== undefined
                  ? listAva[0]['url']
                  : 'https://images.pexels.com/photos/459947/pexels-photo-459947.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
              }
            />
          </Tooltip>
          <input
            id="uploadAva"
            type="file"
            onChange={this.onHandleFileChange}
            style={{ display: 'none' }}
          />
          <p><strong>{(data.lastName !== undefined && data.firstName !== undefined) ? (data.firstName + ' ' + data.lastName) : 'No data'}</strong></p>
          <p><strong>{data === undefined ? 'No data' : data.type}</strong></p>
        </div>

        <Row>
          <Form>
            <Col xs={20} xl={10} offset={1}>
              <Form.Item label="Họ:">
                <Input
                  type="text"
                  ref={(Input) => (this._firstName = Input)}
                  defaultValue={data === undefined ? '' : data.firstName}
                />
              </Form.Item>
              <Form.Item label="Tên:">
                <Input
                  type="text"
                  ref={(Input) => (this._lastName = Input)}
                  defaultValue={data === undefined ? '' : data.lastName}
                />
              </Form.Item>
              <Form.Item label="Ngày Sinh:">
                <Input
                  type="date"
                  ref={(Input) => (this._birthDay = Input)}
                  defaultValue={data === undefined ? '' : data.birthDay}
                />
              </Form.Item>
              <Form.Item label="Giới Tính:">
                <Select
                  defaultValue={data.gender}
                  ref={(Select) => (this._gender = Select)}
                  onSelect={this.getGender}
                >
                  {gender.map((i) => (
                    <Option key={i.key} value={i.key}>
                      {i.label}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item label="Địa Chỉ:">
                <Input
                  type="password"
                  addonAfter={
                    <a
                      className="col-1"
                      data-toggle="collapse"
                      href="#addressCollapse"
                      role="button"
                      aria-expanded="false"
                      aria-controls="addressCollapse"
                    >
                      <i className="fas fa-edit" />
                    </a>
                  }
                  disabled
                />
                <Address />
              </Form.Item>
            </Col>
            <Col xs={20} xl={10} offset={1}>
              <Form.Item label="Email:">
                <Input
                  type="email"
                  ref={(Input) => (this._email = Input)}
                  defaultValue={data === undefined ? '' : data.email}
                />
              </Form.Item>
              <Form.Item label="Số Điện Thoại: ">
                <Input
                  type="phone"
                  ref={(Input) => (this._phone = Input)}
                  defaultValue={data === undefined ? '' : data.phone}
                />
              </Form.Item>
              <Form.Item label="Loại Tài Khoản:">
                <Select
                  defaultValue={data.type}
                  ref={(Select) => (this._type = Select)}
                  onSelect={this.getType}
                >
                  {type.map((i) => (
                    <Option key={i.key} value={i.value}>
                      <span style={{ color: i.color }}>
                        {i.label}
                      </span>
                    </Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item label="Trạng Thái:">
                <Select
                  defaultValue={data.status}
                  ref={(Select) => (this._status = Select)}
                  onSelect={this.getStatus}
                >
                  {status.map((i) => (
                    <Option key={i.key} value={i.value}>
                      <span style={{ color: i.color }}>
                        {i.label}
                      </span>
                    </Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item label="Mật Khẩu:">
                <Input
                  type="password"
                  addonAfter={
                    <a
                      className="col-1"
                      data-toggle="collapse"
                      href="#passwordCollapse"
                      role="button"
                      aria-expanded="false"
                      aria-controls="passwordCollapse"
                    >
                      <i className="fas fa-edit" />
                    </a>
                  }
                  disabled
                />
                <Password />
              </Form.Item>
            </Col>
          </Form>
        </Row>
        <Col span={22} style={{ marginTop: '5%' }}>
          <Row type="flex" justify="center">
            <Col offset={2}>
              <Button
                size="default"
                type="primary"
                style={{ borderRadius: '4px' }}
                onClick={() => {
                  this.updateUser();
                }}
              >
                Lưu
              </Button>
            </Col>
            <Col offset={2}>
              <Button
                size="default"
                type="danger"
                style={{ borderRadius: '4px' }}
                onClick={() => history.push(url)}
              >
                Hủy
              </Button>
            </Col>
          </Row>
        </Col>
      </div>
    );
  }
}

// const WrappedAdminProfile = Form.create({ name: 'Edit Profile' })(AdminProfile);

function mapStateToProps(state) {
  const { Auth, admin } = state;
  return { Auth, admin };
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getProfile,
      updateProfile,
      getAvatar,
      receiveUpdateProfile,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminProfile);
