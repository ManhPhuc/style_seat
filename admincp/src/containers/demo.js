import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import './user-profile-form.css';
import { connect } from 'react-redux';
import { Address } from '../address';
import Select from 'react-select';
import * as API from '../../services/api';
import {
  getProfile,
  getServices,
  updateProfile,
  updateServiceskill,
  getServicesSkill,
  getAvatar,
} from '../../redux/actions';

const gender = [{ label: 'Male', value: 0 }, { label: 'Female', value: 1 }];
const env = process.env.NODE_ENV || 'development';
const config = require('../../config/config.json')[env];
let listservices,
  listServicesSkill,
  listImages,
  listAva = [];

class UserProfileForm extends Component {
  constructor() {
    super();

    this.state = {
      avatar: {},
    };
  }

  componentWillMount() {
    this.props.getProfile();
    this.props.getServices();
    this.props.getServicesSkill();
    this.props.getAvatar();
  }

  submituserRegistrationForm = (e) => {
    // e.preventDefault();
    const fields = {
      firstName: this._fname.value,
      lastName: this._lname.value,
      birthDay: this._dob.value,
      gender: this._gender.state.value['value'],
      address: this.props.getaddress.street,
      ctid: this.props.getaddress.data,
    };
    let ssPut = [];
    this._serviceskill.state['value'].map((service) =>
      ssPut.push({
        svid: service.value,
      })
    );
    this.props.updateServiceskill(ssPut);
    this.props.updateProfile(fields);
  };

  onHandleFileChange = (e) => {
    let formData = new FormData();
    if (e.target.id === 'uploadAva') {
      formData.append('avatar', e.target.files[0]);
    }
    const link = config.base_api + '/images';
    API.postImage(formData, link).then((res) => {
      alert(res);
      // this.props.getImages();
    });
  };

  addAvatar = (e) => {
    e.preventDefault();
    const ele = document.getElementById('uploadAva');
    ele.click(e);
  };

  validateForm() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (typeof fields['email'] !== 'undefined') {
      var pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(fields['email'])) {
        formIsValid = false;
        errors['email'] = '*Please enter valid email.';
      }
    }

    if (!fields['email']) {
      formIsValid = false;
      errors['email'] = '*Please enter your email.';
    }

    if (typeof fields['phone'] !== 'undefined') {
      if (!fields['phone'].match(/^.*[0-9]$/)) {
        formIsValid = false;
        errors['phone'] = '*Please enter valid phone number.';
      }
    }

    if (!fields['phone']) {
      formIsValid = false;
      errors['phone'] = '*Please enter your phone number.';
    }

    if (typeof fields['password'] !== 'undefined') {
      if (!fields['password'].match(/^.*(?=.{8,}).*$/)) {
        formIsValid = false;
        errors['password'] = '*Password must be at least 8 characters.';
      }
    }

    if (!fields['password']) {
      formIsValid = false;
      errors['password'] = '*Please enter your password.';
    }

    if (fields['repassword'] !== fields['password']) {
      formIsValid = false;
      errors['repassword'] = '*The passwords you entered do not match.';
    }

    if (!fields['repassword']) {
      formIsValid = false;
      errors['repassword'] = '*Please enter your repassword.';
    }

    this.setState({
      errors: errors,
    });
    return formIsValid;
  }

  render() {
    const { profile } = this.props.profile;
    const { services } = this.props.services;
    const { ss } = this.props.servicesskill;
    const { data } = this.props.data;
    const { images } = this.props.images;
    let exist = false;
    if (profile !== undefined) exist = true;
    if (services !== undefined) {
      listservices = [];
      services.map((service) =>
        listservices.push({
          value: service.svid,
          label: service.serviceName,
        })
      );
    }
    if (ss !== undefined) {
      listServicesSkill = [];
      ss.map((service) => listServicesSkill.push(service.svid));
    }
    if (images !== undefined) {
      listImages = [];
      listAva = [];
      images.map((image) => {
        if (image['isAvatar'] === 0) {
          listImages.push(image);
        }
        listAva.push(image);
      });
    }
    return (
      <div>
        <div className="container ">
          <div className="col-md-12">
            <div className="justify-content-start row mt-5">
              <div className="col-4">
                <img
                  className="rounded-circle img-responsive pull-right img-thumbnail"
                  alt="Avatar"
                  src={
                    listAva[0] !== undefined
                      ? listAva[0]['url']
                      : 'https://images.pexels.com/photos/459947/pexels-photo-459947.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
                  }
                  style={{ width: '150px', height: '150px' }}
                />
                <div className="pull-right" onClick={this.addAvatar}>
                  <i className="fas fa-plus-circle fa-lg" />
                </div>
                <input
                  id="uploadAva"
                  type="file"
                  onChange={this.onHandleFileChange}
                  style={{ display: 'none' }}
                />
              </div>
              <div className="col-7">
                <div className="row">
                  <h3 className="mt-3">Worker's name</h3>
                </div>
                <div className="row">
                  <h6>Hair cut</h6>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-12" id="nav-signup">
            <div className="tab-content py-3 px-5 px-sm-0">
              <h4 className="titleSignup">Worker Information</h4>
              <div className="row">
                <div className="col">
                  <div
                    className="tab-pane fade show active"
                    role="tabpanel"
                    aria-labelledby="nav-client-tab"
                  >
                    <form
                      className="form-horizontal"
                      name="userRegistrationForm"
                      onSubmit={this.submituserRegistrationForm}
                    >
                      <fieldset>
                        <div className="form-group row">
                          <label className="col-sm-3 offset-sm-1 col-form-label">
                            <div className="pull-left">First Name *</div>
                          </label>
                          {exist ? (
                            <input
                              type="text"
                              className="form-control col-sm-6"
                              defaultValue={profile['firstName']}
                              ref={(input) => (this._fname = input)}
                            />
                          ) : (
                            <input
                              type="text"
                              className="form-control col-sm-6"
                              defaultValue="First Name"
                              ref={(input) => (this._fname = input)}
                            />
                          )}
                        </div>

                        <div className="form-group row">
                          <label className="col-sm-3 offset-sm-1 col-form-label">
                            <div className="pull-left">Last Name *</div>
                          </label>
                          {exist ? (
                            <input
                              type="text"
                              className="form-control col-sm-6"
                              defaultValue={profile['lastName']}
                              ref={(input) => (this._lname = input)}
                            />
                          ) : (
                            <input
                              type="text"
                              className="form-control col-sm-6"
                              defaultValue="Last Name"
                              ref={(input) => (this._lname = input)}
                            />
                          )}
                        </div>

                        <div className="form-group row">
                          <label
                            htmlFor="inputPhone"
                            className="col-sm-3 offset-sm-1 col-form-label"
                          >
                            <div className="pull-left">Gender *</div>
                          </label>
                          <Select
                            className="col-sm-6 react-select-container"
                            options={gender}
                            defaultValue={
                              profile !== undefined
                                ? gender.filter(
                                    (option) =>
                                      option.value === profile['gender']
                                  )
                                : true
                            }
                            ref={(Select) => (this._gender = Select)}
                          />
                        </div>

                        <div className="form-group row">
                          <label className="col-sm-3 offset-sm-1 col-form-label">
                            <div className="pull-left">Birth Day *</div>
                          </label>
                          <input
                            type="date"
                            name="dob"
                            className="form-control col-sm-6"
                            placeholder="Phone Number"
                            defaultValue={
                              profile !== undefined
                                ? profile['birthDay']
                                : '1990-02-02'
                            }
                            ref={(input) => (this._dob = input)}
                          />
                        </div>
                      </fieldset>
                    </form>
                  </div>
                </div>
                <div className="col">
                  <div
                    className="tab-pane fade show active"
                    role="tabpanel"
                    aria-labelledby="nav-client-tab"
                  >
                    <form
                      className="form-horizontal"
                      name="userRegistrationForm"
                      onSubmit={this.submituserRegistrationForm}
                    >
                      <fieldset>
                        <div className="form-group row">
                          <label
                            htmlFor="inputPhone"
                            className="col-sm-3 offset-sm-1 col-form-label"
                          >
                            <div className="pull-left">Phone *</div>
                          </label>
                          <input
                            type="text"
                            name="phone"
                            className="form-control col-sm-6"
                            placeholder="Phone Number"
                            defaultValue={data.user['phone']}
                            ref={(input) => (this._phone = input)}
                          />
                        </div>
                        <div className="form-group row">
                          <label
                            htmlFor="inputEmail"
                            className="col-sm-3 offset-sm-1 col-form-label"
                          >
                            <div className="pull-left">Email</div>
                          </label>
                          <input
                            type="text"
                            name="email"
                            className="form-control col-sm-6"
                            value={data.user['email']}
                            disabled
                          />
                        </div>
                        <div className="form-group row ">
                          <label
                            htmlFor="inputPassword"
                            className="col-sm-3 offset-sm-1 col-form-label"
                          >
                            <div className="pull-left">Service Skill *</div>
                          </label>
                          {ss !== undefined ? (
                            <Select
                              className="col-sm-6 react-select-container"
                              options={listservices}
                              ref={(Select) => (this._serviceskill = Select)}
                              id={console.log(listservices)}
                              defaultValue={
                                listservices !== undefined
                                  ? listservices.filter((service) =>
                                      listServicesSkill.includes(service.value)
                                    )
                                  : true
                              }
                              isMulti
                            />
                          ) : (
                            true
                          )}
                        </div>
                      </fieldset>
                    </form>
                  </div>
                </div>
              </div>
              <div style={{ marginBottom: '5%' }} />
              {/* <div className="form-group row">
                <div className="input-group input-file">
                  <label
                    htmlFor="inputPassword"
                    className="col-sm-3 offset-sm-1 col-form-label"
                  >
                    <div className="pull-left">Image *</div>
                  </label>
                  <input
                    id="upload"
                    type="file"
                    className="col-sm-3 input-group input-file"
                    name="images"
                    placeholder="Choose a file..."
                    style={{ width: '80%' }}
                    multiple
                    onChange={this.onHandleFileChange}
                  />
                </div>
              </div> */}
            </div>
            <div className="tab-content py-3 px-5 px-sm-0">
              <div
                className="tab-pane fade show active"
                role="tabpanel"
                aria-labelledby="nav-client-tab"
              >
                <h4 className="titleSignup">Work Location</h4>
                <form
                  className="form-horizontal"
                  name="userRegistrationForm"
                  onSubmit={this.submituserRegistrationForm}
                >
                  <fieldset>
                    <div className="form-group row">
                      <label className="col-sm-3 offset-sm-1 col-form-label">
                        <div className="pull-left">Address</div>
                      </label>
                      <input
                        type="text"
                        name="address"
                        className="form-control col-sm-6"
                        defaultValue={
                          profile !== undefined ? profile['address'] : true
                        }
                        disabled
                      />
                      <a
                        className="col-1"
                        data-toggle="collapse"
                        href="#collapseExample"
                        role="button"
                        aria-expanded="false"
                        aria-controls="collapseExample"
                      >
                        <i className="fas fa-edit" />
                      </a>
                    </div>
                  </fieldset>
                  <Address />
                </form>
              </div>
              <div style={{ marginBottom: '5%' }} />
            </div>
            <div className="form-group row offset-sm-5">
              <button
                onClick={this.submituserRegistrationForm}
                className="col-sm-3 btn btn-primary "
              >
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { getaddress, profile, data, services, servicesskill, images } = state;
  return { getaddress, profile, data, services, servicesskill, images };
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getProfile,
      getServices,
      updateProfile,
      updateServiceskill,
      getServicesSkill,
      getAvatar,
    },
    dispatch
  );

const connectedUserProfileFormPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserProfileForm);
export { connectedUserProfileFormPage as UserProfileForm };
