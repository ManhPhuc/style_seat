const width = 1000;
const height = 350;
const colors = ['#BAA6CA', '#B7DCFA', '#FFE69A', '#788195'];
const datas = [
  { name: 'January', sales: 4000},
  { name: 'February', sales: 3000},
  { name: 'March', sales: 4500},
  // { name: 'Page D', uv: 2780, pv: 3908, amt: 2000 },
  // { name: 'Page E', uv: 1890, pv: 4800, amt: 2181 },
  // { name: 'Page F', uv: 2390, pv: 3800, amt: 2500 },
  // { name: 'Page G', uv: 3490, pv: 4300, amt: 2100 },
];

const SimpleAreaChart = {
  componentName: 'SimpleAreaChart',
  key: 'SimpleAreaChart',
  title: 'Simple Area Chart',
  width,
  height,
  colors,
  datas,
};
export {
  SimpleAreaChart,
};
