import React from 'react';
import Async from '../../../helpers/asyncComponent';


const SimpleAreaChart = (props) => (
  <Async
    load={import(/* webpackChunkName: "rechartx-simpleAreaChart" */ './simpleAreaChart')}
    componentProps={props}
  />
);


export {
  SimpleAreaChart,
};
