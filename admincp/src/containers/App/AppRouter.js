import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import asyncComponent from '../../helpers/AsyncFunc';

const routes = [
  {
    path: '',
    component: asyncComponent(() => import('../newDashBoard')),
  },
  // {
  //   path: 'dashBoard',
  //   component: asyncComponent((props) => import('../dashboard')),
  // },
  {
    path: 'adminProfile',
    component: asyncComponent(() => import('../adminProfile')),
  },
  {
    path: 'User',
    component: asyncComponent(() => import('../users')),
  },
  {
    path: 'user/user-profile/:id',
    component: asyncComponent(() => import('../Page/user-profile')),
  },
  {
    path: 'user/add-user',
    component: asyncComponent(() => import('../Page/add-user')),
  },
  {
    path: 'order',
    component: asyncComponent(() => import('../orders')),
  },
  // {
  //   path: 'order',
  //   component: asyncComponent(() => import('../order')),
  // },

  {
    path: 'services/update-service/:id',
    component: asyncComponent(() => import('../Page/update-service')),
  },
  {
    path: 'services',
    component: asyncComponent(() => import('../services')),
  },
  {
    path: 'services/add-service',
    component: asyncComponent(() => import('../Page/add-service')),
  },
];

class AppRouter extends Component {
  render() {
    const { url, style } = this.props;
    return (
      <div style={style}>
        {routes.map((singleRoute) => {
          const { path, exact, ...otherProps } = singleRoute;
          return (
            <Route
              exact={exact === false ? false : true}
              key={singleRoute.path}
              path={`${url}/${singleRoute.path}`}
              {...otherProps}
            />
          );
        })}
      </div>
    );
  }
}

export default AppRouter;
