import { HTTP } from '../common';

export const postData = async (data, link) =>
  await HTTP.Post(link, data)
    .then((res) => {
      const data = res.json();
      return data;
    })
    .catch((error) => error);

export const postImage = async (data, link) =>
  await HTTP.PostImage(link, data)
    .then((res) => {
      const data = res.json();
      return data;
    })
    .catch((error) => error);

export const getData = async (link) =>
  await HTTP.Get(link)
    .then((res) => {
      const data = res.json();
      return data;
    })
    .catch((error) => error);

export const putData = async (data, link) =>
  await HTTP.Put(link, data)
    .then((res) => {
      const data = res.json();
      return data;
    })
    .catch((error) => error);

export const loadImage = async (data, link) => {
  await HTTP.loadImage(link, data)
    .then((res) => {
      const data = res.json();
      return data;
    })
    .catch((error) => error);
};
